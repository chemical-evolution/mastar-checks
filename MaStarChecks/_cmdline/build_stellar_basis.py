
import sys, os
import argparse
import joblib

from pprint import pprint
import numpy as np
import pandas as pd
from astropy.io import fits

from MaStarChecks.common import no_traceback
from MaStarChecks.clustering.hierarchical import set_labels
from MaStarChecks.io import write_basis_parameters, write_basis_seds, write_fit3d_basis

__script__ = os.path.basename(__file__).replace(".py", "")

Y_COLUMNS = ["TEFF", "LOGG", "MET", "ALPHAM"]
Y_UNITS = ["log/K", "log/cm/s^2", "[Fe/H]", "[alpha/Fe]"]

# CLUSTER_NUMBER = 15
# CLUSTER_DISTANCE = 5.5
CWD = os.path.abspath(".")
CACHE_PATH = os.path.join(CWD, ".cache")

def _main(cmd_args=sys.argv[1:], random_seed=123):
    parser = argparse.ArgumentParser(description="stellar spectra basis from hierarchical clustering")
    parser.add_argument(
        "cluster_path", metavar="cluster-path",
        help="path from where the hierarchical cluster FITS file will be loaded"
    )
    parser.add_argument(
        "estimator_path", metavar="estimator-path",
        help="path from where the estimator pickle file will be loaded"
    )
    cluster_group = parser.add_mutually_exclusive_group(required=True)
    cluster_group.add_argument(
        "--n-clusters",
        help=f"the number of clusters at which the tree will be trimmed. Several values can be passed to perform several cuts",
        nargs="+",
        type=np.int
    )
    cluster_group.add_argument(
        "--max-distances",
        help=f"the maximum distance allowed for members of the same cluster. Several values can be passed to perform several cuts",
        nargs="+",
        type=np.float
    )
    parser.add_argument(
        "--label",
        help="label to use for the output file name"
    )
    parser.add_argument(
        "--output-path",
        help="path from where to store processed FITS. Defaults to current working directory",
        default=os.path.abspath(CWD)
    )
    parser.add_argument(
        "--use-cache",
        help="whether to run using a cached run. If no cache is found, it will be built in the specified cache path",
        action="store_true"
    )
    parser.add_argument(
        "--cache-path",
        help=f"path where to store/load the cache if --use-cache is passed. Defaults to {CACHE_PATH}",
        default=CACHE_PATH
    )
    parser.add_argument(
        "--fit3d-format",
        help=f"whether to write or not the stellar basis in FIT3D FITS format",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode (meant to test changes in the code)",
        action="store_true"
    )
    args = parser.parse_args(cmd_args)
    if not args.debug:
        sys.excepthook = no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")

    if args.use_cache and not os.path.isdir(args.cache_path): os.mkdir(args.cache_path)

    # TODO: change this to read a FITS file containing a table with the spectra, the catalogue
    #       and the linkage matrix
    f = fits.open(args.cluster_path)
    X = f[0].data[0]
    Xerr = f[0].data[1]
    y = np.column_stack((f[1].data["TEFF"],f[1].data["LOGG"],f[1].data["MET"],f[1].data["ALPHAM"]))
    y[:, 0] = np.log10(y[:, 0])

    mor = joblib.load(args.estimator_path)
    yerr = np.abs(mor["P84"].predict(X)-mor["P16"].predict(X)) / 2
    typical_std = np.median(yerr, axis=0)

    Z = np.asarray(f[2].data.tolist())

    # calculate cuts of the resulting tree at different number of clusters
    iterator = (zip(["n_clusters"]*len(args.n_clusters), args.n_clusters) if args.n_clusters is not None else zip(["max_distance"]*len(args.max_distances), args.max_distances))
    for par_name, cluster_cut in iterator:
        # retrieve cluster labels and the cluster centroids for a number of clusters
        labels, cluster_cent = set_labels(X, Z, **{par_name: cluster_cut})
        n_clusters = cluster_cent.size
        # build basis using the cluster centroids
        X_basis = X[cluster_cent, :]
        y_basis = y[cluster_cent, :]
        # add normalization flux for the basis parameters
        y_basis = np.column_stack((y_basis,f[1].data["FNORM"][cluster_cent]))

        # dump FITS files for the basis spectra and physical parameters
        if args.label is not None:
            output_label = "{}-{}".format(n_clusters, args.label).lower()
        else:
            output_label = "{}".format(n_clusters)
        pdfs_hdus = write_basis_parameters(
            params=y,
            errors=yerr,
            params_labels=labels,
            weights=f[1].data["VCORR"],
            columns=Y_COLUMNS,
            units=Y_UNITS,
            steps=typical_std/3,
            label=output_label
        )
        wavelengths = ((np.arange(f[0].header["NAXIS1"])+1)-f[0].header["CRPIX1"]) * f[0].header["CDELT1"] + f[0].header["CRVAL1"]
        basis_hdus = write_basis_seds(
            seds=X_basis,
            wavelengths=wavelengths,
            norm_wlength=5500,
            basis_params=y_basis,
            columns=Y_COLUMNS+["FNORM"],
            units=Y_UNITS+["erg/s/cm^2/AA"],
            label=output_label
        )
        if args.fit3d_format:
            basis_fit3d = write_fit3d_basis(
                seds=X_basis,
                wavelengths=wavelengths,
                norm_wlength=5500,
                basis_params=y_basis,
                columns=Y_COLUMNS+["FNORM"],
                units=Y_UNITS+["erg/s/cm^2/AA"],
                label=output_label
            )
        f[1].columns.add_col(fits.Column(name=f"CLUSTER_{n_clusters}", array=labels, format="K"))
    labelled_catalogue = fits.HDUList([fits.PrimaryHDU(),f[1]])
    labelled_catalogue.writeto(f"labelled-catalogue-{output_label}.fits.gz", overwrite=True)

    return None
