
from distutils.log import error
import sys, os
import re
import argparse

import joblib
from math import comb
from pprint import pprint
import numpy as np
import pandas as pd
from scipy.cluster import hierarchy
from astropy.io import fits

from MaStarChecks.constants import TESTING_SIZE

from MaStarChecks.constants import ERROR_FRACTION, CARDS
from MaStarChecks.common import no_traceback
from MaStarChecks.clustering.distances import pairwise_chisq
from MaStarChecks.io import load_libraries


__script__ = os.path.basename(__file__).replace(".py", "")

Y_COLUMNS = ["TEFF", "LOGG", "MET", "ALPHAM"]

LIBRARY_KIND = "processed"
METHOD = "complete"
CLUSTER_SIZE_THRESHOLD = 20

# CLUSTER_NUMBER = 15
# CLUSTER_DISTANCE = 5.5
PLOT = 0
CWD = os.path.abspath(".")
CACHE_PATH = os.path.join(CWD, ".cache")


def snr_cut(wavelength, spectra, errors, catalogue, windows=[(3600, 4000), (4500, 5000), (8500, 9500)], min_snr=100):

    mask_clean = (spectra>0).all(axis=1)
    for range_wl in windows:
        mask_wl = (range_wl[0] <= wavelength) & (wavelength <= range_wl[1])
        snr_win = np.nanmean(np.divide(spectra.values, errors.values, where=errors.values!=0)[:, mask_wl], axis=1)

        mask_clean &= snr_win > min_snr

    catalogue_clean = catalogue.loc[mask_clean]
    spectra_clean = spectra.loc[mask_clean]
    errors_clean = errors.loc[mask_clean]

    return spectra_clean, errors_clean, catalogue_clean


def _main(cmd_args=sys.argv[1:], random_seed=123):
    parser = argparse.ArgumentParser(description="stellar spectra basis from hierarchical clustering")
    parser.add_argument(
        "labels", metavar="library-labels",
        help="the library labels to look for when loading the processed stellar spectra libraries",
        nargs="+"
    )
    parser.add_argument(
        "--library-kind",
        help=f"the overal library label. Defaults to {LIBRARY_KIND}",
        default=LIBRARY_KIND
    )
    parser.add_argument(
        "--clustering-method",
        help=f"the method to use to determine the clusters. Defaults to {METHOD}",
        default=METHOD
    )
    parser.add_argument(
        "--input-path",
        help="path from where the input files will be loaded. Defaults to current working directory",
        default=os.path.abspath(CWD)
    )
    parser.add_argument(
        "--output-path",
        help="path from where to store processed FITS. Defaults to current working directory",
        default=os.path.abspath(CWD)
    )
    parser.add_argument(
        "--use-cache",
        help="whether to run using a cached run. If no cache is found, it will be built in the specified cache path",
        action="store_true"
    )
    parser.add_argument(
        "--cache-path",
        help=f"path where to store/load the cache if --use-cache is passed. Defaults to {CACHE_PATH}",
        default=CACHE_PATH
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode (meant to test changes in the code)",
        action="store_true"
    )
    parser.add_argument(
        "-t", "--testing",
        help="run in testing mode (meant to test the data sets)",
        action="store_true"
    )
    parser.add_argument(
        "-n", "--testing-size",
        help=f"when running in testing and/or debugging mode, the size of the sample spectra in each library. Defaults to {TESTING_SIZE}",
        default=TESTING_SIZE
    )
    parser.add_argument(
        "--plot",
        help=f"whether to plot or not the result. Defaults to {PLOT}",
        type=np.int,
        default=PLOT
    )
    args = parser.parse_args(cmd_args)
    if not args.debug:
        sys.excepthook = no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")
    if args.plot > 0:
        import matplotlib.pyplot as plt
        import seaborn as sns
        sns.set(context="talk", style="ticks", palette="colorblind", color_codes=True)

    if args.use_cache and not os.path.isdir(args.cache_path): os.mkdir(args.cache_path)

    spectra, catalogues = load_libraries(
        labels=args.labels,
        spectra_types=["FLUX", "ERROR"],
        kind=args.library_kind,
        libraries_path=args.input_path,
        use_cache=args.use_cache,
        cache_path=args.cache_path
    )
    catalogue = pd.concat([c for _, c in catalogues.items()], axis="index", ignore_index=True)
    X = pd.concat([s["FLUX"] for _, s in spectra.items()], axis="index", ignore_index=True)
    Xerr = pd.concat([s["ERROR"] for _, s in spectra.items()], axis="index", ignore_index=True)

    # apply SNR cuts to spectra, errors and catalogue
    X, Xerr, catalogue = snr_cut(wavelength=X.columns, spectra=X, errors=Xerr, catalogue=catalogue, min_snr=100)

    catalogue = catalogue.dropna(subset=Y_COLUMNS, how="any", axis="index")
    # shorten sample randomly for testing and debugging
    if args.testing:
        catalogue = catalogue.sample(n=args.testing_size, random_state=random_seed)
    # select stellar parameter properties
    X = X.loc[catalogue.index]
    y = catalogue.copy().loc[:, Y_COLUMNS]

    labels_ = list(map(str.lower, args.labels))

    # determine distance vector & matrix
    DISTANCES_PATH = os.path.join(args.output_path, "distances-{}.fits.gz".format("-".join(labels_)))
    if args.use_cache and os.path.isfile(DISTANCES_PATH):
        distance_vector = fits.getdata(DISTANCES_PATH, hdu=0)
    else:
        distance_vector = pairwise_chisq(X.values, Xerr.values, n=comb(X.values.shape[0], 2))

        dist_hdu = fits.PrimaryHDU(distance_vector)
        dist_hdu.writeto(DISTANCES_PATH, overwrite=True)

    # build linkage matrix
    CLUSTERS_PATH = os.path.join(args.output_path, "hierarchical-cluster-{}.fits.gz".format("-".join(labels_)))
    if args.use_cache and os.path.isfile(CLUSTERS_PATH):
        Z = fits.getdata(CLUSTERS_PATH, hdu=2)
    else:
        Z = hierarchy.linkage(distance_vector, method="complete", optimal_ordering=True)

        # write spectra
        wl = X.columns
        delt = np.diff(wl)[0]
        if not np.all(np.diff(wl)==delt):
            print("ERROR: the wavelength sampling is not uniform")
        spectra_hdr = fits.Header([
            ("CRVAL1", wl[0]),
            ("CDELT1", delt),
            ("CRPIX1", 1),
            ("CTYPE1", "wavelength"),
            ("CUNIT1", "AA")
        ])
        spectra_hdu = fits.PrimaryHDU(data=np.stack((X.values,Xerr.values), axis=0), header=spectra_hdr)
        spectra_hdu.name = "SPECTRA"

        # write catalogue
        units = dict(CARDS)
        names = list(units.keys())
        fmts = {name: "E" if catalogue.dtypes[name]!="object" else f"A{catalogue[name].apply(lambda s: len(s)).max()}" for name in names}
        columns = []
        for name in catalogue.columns:
            unit = re.findall(r"(\[.*?\])", units[name])
            unit = unit[0] if unit else ""
            unit = unit if name in ["MET","ALPHAM"] else unit.strip("[]")
            columns.append(fits.Column(name=name, array=catalogue[name].values, format=fmts[name], unit=unit))
        catalogue_hdu = fits.BinTableHDU.from_columns(columns)
        catalogue_hdu.name = "CATALOGUE"

        # write hierarchical cluster
        names = ["CLUSTERI", "CLUSTERJ", "DISTANCE", "COUNTS"]
        fmts = {name: "K" if name!="DISTANCE" else "E" for name in names}
        clusters_hdu = fits.BinTableHDU.from_columns([fits.Column(name=name, array=Z[:,j], format=fmts[name]) for j, name in enumerate(names)])
        clusters_hdu.name = "CLUSTER"

        hdus = fits.HDUList([spectra_hdu,catalogue_hdu,clusters_hdu])
        hdus.writeto(CLUSTERS_PATH, overwrite=True)

    return None
