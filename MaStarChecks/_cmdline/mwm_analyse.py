import sys
import os
import numpy as np

import joblib
import argparse
from pprint import pprint

from astropy.io import fits

from MaStarChecks.common import no_traceback


COSHA_PATH = os.path.join(os.path.dirname(os.path.dirname(os.path.abspath(__file__))), "etc", "cosha")
COSHA_VERSION = "v3.1.2"
COSHA_SUBVERSION = "cleaned-v2-gsl-dust"
CWD = os.path.abspath(".")

def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="analysis of MWM stellar spectrum using CoSHA")
    parser.add_argument(
        "input_path", metavar="input-path",
        help="path to FITS file containing MWM spectrum"
    )
    parser.add_argument(
        "--cosha-path",
        help=f"path where CoSHA estimators are located. Defaults to '{COSHA_PATH}'",
        default=COSHA_PATH
    )
    parser.add_argument(
        "--cosha-version",
        help=f"version of CoSHA to use. Defaults to '{COSHA_VERSION}'",
        default=COSHA_VERSION
    )
    parser.add_argument(
        "--cosha-subversion",
        help=f"subversion of CoSHA to use. Defaults to '{COSHA_SUBVERSION}'",
        default=COSHA_SUBVERSION
    )
    parser.add_argument(
        "--force-analyse",
        help="whether to force analysis or not. Defaults to False",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode (meant to test changes in the code)",
        action="store_true"
    )
    parser.add_argument(
        "-v", "--verbose",
        help="print useful information on screen",
        action="store_true"
    )
    # TODO: add plot option showing the parameter space with MaStar and the resulting parameter
    # TODO: showing isochrones
    args = parser.parse_args(cmd_args)
    if not args.debug:
        sys.excepthook = no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")
    
    with fits.open(args.input_path, mode="update") as sed_fits:
        if args.force_analyse or not "COSHA" in sed_fits[0].header:
            cosha = joblib.load(os.path.join(args.cosha_path, f"estimator-mor-{args.cosha_version}-{args.cosha_subversion}.pk"))

            try:
                (teff, logg, feh, afe, ebv) = cosha["mode"].predict(np.atleast_2d(sed_fits[1].data["FLUX"])).ravel()
            except ValueError:
                (teff, logg, feh, afe) = cosha["mode"].predict(np.atleast_2d(sed_fits[1].data["FLUX"])).ravel()
                ebv = -9999
            teff = 10**teff

            sed_fits[0].header["COSHA"] = (args.cosha_version, "CoSHA version")
            sed_fits[0].header["TEFF"] = (teff, "[K] effective temperature from CoSHA")
            sed_fits[0].header["LOGG"] = (logg, "[log/cm/s^2] surface gravity from CoSHA")
            sed_fits[0].header["FEH"] = (feh, "[Fe/H] metallicity from CoSHA")
            sed_fits[0].header["ALPHAM"] = (afe, "[alpha/Fe] alpha-to-iron abundance from CoSHA")
            sed_fits[0].header["EBV"] = (ebv, "[mag] E(B-V) color excess from CoSHA")
            sed_fits[0].header.add_comment("", before="COSHA")
            sed_fits[0].header.add_comment("*** CoSHA analysis ***", before="COSHA")
            sed_fits[0].header.add_comment("", before="COSHA")

            sed_fits.writeto(sed_fits.filename(), overwrite=True, output_verify="silentfix")
