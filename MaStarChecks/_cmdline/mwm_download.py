import os
import sys
import argparse
import numpy as np
import subprocess
from pprint import pprint
from astropy.io import fits
from astropy.table import Table
from enum import IntFlag, auto
import warnings
warnings.filterwarnings("ignore")

from MaStarChecks.common import no_traceback


VERSION = 'v6_0_4'
SUBVERSION = ''
MWM_SAS_URL = "https://data.sdss.org/sas/sdss5/bhm/boss/spectro/redux"


class ZWarning(IntFlag):
    SKY = auto()
    LITTLE_COVERAGE = auto()
    SMALL_DELTA_CHI2 = auto()
    NEGATIVE_MODEL = auto()
    MANY_OUTLIERS = auto()
    Z_FITLIMIT = auto()
    NEGATIVE_EMISSION = auto()
    UNPLUGGED = auto()
    BAD_TARGET = auto()
    NODATA = auto()


def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
            description="Download MWM stellar spectra")
    parser.add_argument(
        "catalogue_path", metavar="PATH",
        help=f"path to download requested catalogue"
    )
    parser.add_argument(
        "--spectra-path", metavar="PATH",
        help=f"path where spectra will be downloaded. Defaults to 'catalogue-path'"
    )
    parser.add_argument(
        "--version",
        help=f"version of the catalogue to download. Defaults to '{VERSION}'",
        default=VERSION
    )
    parser.add_argument(
        "-s", "--sub-version",
        help=f"sub-version of the catalogue to download. Defaults to '{SUBVERSION}'",
        default=SUBVERSION
    )
    parser.add_argument(
        "--force-catalogue",
        help=f"whether to force download the catalogue or not. Defaults to False",
        action="store_true"
    )
    parser.add_argument(
        "--no-spectra",
        help=f"whether to try to download stellar spectra in the catalogue or not. Defaults to False",
        action="store_true"
    )
    parser.add_argument(
        "--force-spectra",
        help=f"whether to force download spectra or not. Defaults to False",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode (meant to test changes in the code)",
        action="store_true"
    )
    parser.add_argument(
        "-v", "--verbose",
        help="print useful information on screen",
        action="store_true"
    )
    args = parser.parse_args(cmd_args)

    if not args.debug:
        sys.excepthook = no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")
    
    args.catalogue_path = os.path.abspath(args.catalogue_path)
    if args.spectra_path is None:
        args.spectra_path = args.catalogue_path
    else:
        args.spectra_path = os.path.abspath(args.spectra_path)

    version_ = args.version.replace('/','-')
    version_full = version_ + args.sub_version

    os.makedirs(args.catalogue_path, exist_ok=True)
    os.makedirs(args.spectra_path, exist_ok=True)

    catalogue_path = os.path.join(args.catalogue_path, args.version)
    spectra_path = os.path.join(args.spectra_path, args.version, 'spectra')

    os.makedirs(catalogue_path, exist_ok=True)
    os.makedirs(spectra_path, exist_ok=True)

    catalogue_name = f"spAll-{version_full}.fits"
    catalogue_file = os.path.join(catalogue_path, catalogue_name)

    if args.force_catalogue or not os.path.isfile(catalogue_file):
        subprocess.run([
            "wget",
            "--user", "sdss5",
            "--password", "panoptic-5",
            f"{MWM_SAS_URL}/{args.version}/{catalogue_name}",
            "-P", catalogue_path
        ])

    if os.path.isfile(catalogue_file):
        catalogue_fits = fits.open(catalogue_file, memmap=True)
        table_data = Table(catalogue_fits[1].data)
        mask = (
            (np.char.strip(table_data["OBJTYPE"]) != "SKY") &
            (np.char.strip(table_data["OBJTYPE"]) != "SPECTROPHOTO_STD") &
            (np.char.strip(table_data["CLASS"]) == "STAR") &
            (table_data["ZWARNING"] != (ZWarning(0) | ZWarning(7) | ZWarning(8) | ZWarning(9))) &
            (np.char.strip(table_data["PROGRAMNAME"]) == "MWM") | (np.char.strip(table_data["PROGRAMNAME"]) == "MWM_30min")
        )
        table_stars = table_data[mask]
    else:
        raise FileNotFoundError(f"missing catalogue '{catalogue_file}'")

    stars_path = os.path.dirname(catalogue_file)
    stars_file = os.path.join(stars_path, f"stars-{args.version}.fits")
    if args.force_catalogue or not os.path.isfile(stars_file):
        spec_names = [f"spec-{plate:05d}-{mjd}-{catid:011d}.fits" for catid, mjd, plate in table_stars.iterrows("CATALOGID", "MJD", "PLATE")]
        table_stars.add_column(spec_names, name="FILENAME")
        table_stars.write(stars_file, overwrite=True)

    if not args.no_spectra:
        missing_spectra = []
        for catid, mjd, plate in table_stars.iterrows("CATALOGID", "MJD", "PLATE"):
            spec_name = f"spec-{plate:05d}-{mjd}-{catid:011d}.fits"
            spec_file = os.path.join(spectra_path, spec_name)
            print(f"{MWM_SAS_URL}/{args.version}/spectra/full/{plate}p/{mjd}/{spec_name}")
            if args.force_spectra or not os.path.isfile(spec_file):
                status = subprocess.run([
                    "wget",
                    "--user", "sdss5",
                    "--password", "panoptic-5",
                    f"{MWM_SAS_URL}/{args.version}/spectra/full/{plate}p/{mjd}/{spec_name}",
                    "-P", spectra_path
                ])
                if status.returncode != 0:
                    missing_spectra.append(spec_file)
            
        print("missing spectra:")
        pprint(missing_spectra)