import sys
import os
import argparse
import numpy as np
from scipy.interpolate import interp1d
import matplotlib.pyplot as plt
from pprint import pprint
from astropy.io import fits
from astropy.table import Table
from astropy import units as u
from dust_extinction.parameter_averages import F99

from MaStarChecks.common import no_traceback
from MaStarChecks.constants import WAVELENGTH_NORM, SPECTRA_TYPES
from MaStarChecks.preprocessing import downgrade_resolution


CWD = os.path.abspath(".")


def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(
        description="pre-processing of MWM stellar spectrum")
    parser.add_argument(
        "input_spectrum", metavar="PATH",
        help="path to FITS file containing MWM spectrum"
    )
    parser.add_argument(
        "input_catalogue", metavar="PATH",
        help="path to FITS file containing the MWMW catalogue"
    )
    parser.add_argument(
        "--wavelength-sampling", metavar=("wl_ini", "wl_fin", "wl_step"),
        help="wavelength sampling of the final spectrum",
        nargs=3,
        type=np.float
    )
    parser.add_argument(
        "--wavelength-norm", metavar=("wl_norm_ini", "wl_norm_fin"),
        help=f"the normalization windows in wavelength. The processed fluxes (and errors) will be normalized using the median within this range. Defaults to {WAVELENGTH_NORM}",
        nargs=2,
        default=WAVELENGTH_NORM,
        type=np.float
    )
    parser.add_argument(
        "--reference-lsf", metavar="lsf_path",
        help="reference LSF spectrum"
    )
    parser.add_argument(
        "--remove-dust",
        help="whether to remove MW foreground dust or not. Defaults to False",
        action="store_true"
    )
    parser.add_argument(
        "--extrapolate-const",
        help="the value to use when extrapolating during resampling of the spectra. Defaults to the extrema values",
        type=np.float
    )
    parser.add_argument(
        "-f", "--fill-missing",
        help="fill the missing values from the libraries. By default all flux values <= 0 are flagged as missing",
        action="store_true"
    )
    parser.add_argument(
        "-o", "--output-path",
        help="path where to store processed FITS. Defaults to current working directory",
        default=CWD
    )
    parser.add_argument(
        "--force-preprocess",
        help="whether to force preprocessing if file already exists. Defaults to False",
        action="store_true"
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode (meant to test changes in the code)",
        action="store_true"
    )
    parser.add_argument(
        "-v", "--verbose",
        help="print useful information on screen",
        action="store_true"
    )
    parser.add_argument(
        "--plot", type=int,
        help=f"whether to plot spectra on display (1), on .png file (2) or not plot (0)",
        choices=(0,1,2),
        default=0
    )
    args = parser.parse_args(cmd_args)
    if not args.debug:
        sys.excepthook = no_traceback
    else:
        pprint("COMMAND LINE ARGUMENTS")
        pprint(f"{args}\n")
    
    if args.plot == 1 and not args.verbose:
        args.plot == 2
    
    # check if preprocessed file exists and pre-process only if does not or forced
    sed_name = os.path.basename(args.input_spectrum).replace(".fits", "").replace(".gz", "")
    out_path = os.path.join(args.output_path, f"{sed_name}.cosha.fits.gz")
    if args.force_preprocess or not os.path.isfile(out_path):

        catalogue = Table(fits.getdata(args.input_catalogue, ext=1))
        catalogue.add_index("FILENAME")
        sed_fits = fits.open(args.input_spectrum)
        
        # read wavelengths and convert to air if needed
        wl = 10**sed_fits[1].data["LOGLAM"]
        if sed_fits[0].header["VACUUM"]:
            sigma_2 = (1e4/wl)**2
            f = 1.0 + 0.05792105 / (238.0185 - sigma_2) + 0.00167917 / (57.362 - sigma_2)
            wl = wl / f

        # flux
        fl = sed_fits[1].data["FLUX"] * 1e-17
        # error spectrum
        fe = sed_fits[1].data["IVAR"]
        fe = np.sqrt(np.divide(1, fe, where=fe>0.0, out=np.zeros_like(fe))) * 1e-17
        # LSF in AA
        wr = sed_fits[1].data["WRESL"]

        # remove dust
        if args.remove_dust:
            RV = 3.1
            ext = F99(Rv=RV)
            reddening = ext.extinguish(wl.astype(float)*u.AA, Ebv=catalogue.loc[f"{sed_name}.fits"]["SFD_EBV"])
        else:
            reddening = np.ones_like(wl)
        fl = np.divide(fl, reddening, where=reddening!=0, out=np.full_like(fl,fill_value=np.nan,dtype=np.double))

        # resample spectrum to a uniform sampling
        wl_del = np.diff(wl).min()
        wl_uni = np.arange(wl[0], wl[-1]+wl_del, wl_del)
        fl_uni = np.interp(wl_uni, wl, fl, left=args.extrapolate_const, right=args.extrapolate_const)
        fe_uni = np.interp(wl_uni, wl, fe, left=0, right=0)
        wr_uni = np.interp(wl_uni, wl, wr, left=0, right=0)

        if args.reference_lsf is not None:
            # read reference LSF
            ref_lsf = np.loadtxt(args.reference_lsf)
            # interpolate to SED wavelength
            lin_interp = interp1d(*ref_lsf.T, kind="linear", fill_value=args.extrapolate_const)
            lsf = lin_interp(wl_uni)

            # downgrade resolution
            diff_lsf = np.sqrt(lsf**2-wr_uni**2, where=lsf-wr_uni>0, out=np.zeros_like(wr_uni))
            fl_rs = downgrade_resolution(wavelength=wl_uni, spectrum=fl_uni, sigma=diff_lsf / 2.355, verbose=args.verbose)
            fe_rs = downgrade_resolution(wavelength=wl_uni, spectrum=fe_uni, sigma=diff_lsf / 2.355, verbose=args.verbose)
        else:
            diff_lsf = 0
            fl_rs = fl_uni
            fe_rs = fe_uni

        if args.wavelength_sampling is not None:
            # resample spectrum to final sampling
            wl_ini, wl_fin, wl_step = args.wavelength_sampling
            wl_new = np.arange(wl_ini, wl_fin+wl_step, wl_step)
            fl_new = np.interp(wl_new, wl_uni, fl_rs, left=args.extrapolate_const, right=args.extrapolate_const)
            fe_new = np.interp(wl_new, wl_uni, fe_rs, left=0, right=0)
            wr_new = np.interp(wl_new, wl_uni, wr_uni, left=0, right=0)
        else:
            wl_new = wl_uni
            fl_new = fl_uni
            fe_new = fe_uni
            wr_new = wr_uni

        # normalize spectrum
        mask_norm = (wl_new>=args.wavelength_norm[0])&(wl_new<=args.wavelength_norm[1])
        fl_norm = np.nanmedian(fl_new[mask_norm])

        fl_new /= fl_norm
        fe_new /= fl_norm

        # write output spectrum
        if args.plot > 0:
            # TODO: add inset showing H&K or CaII triplet
            # TODO: add figure showing the resulting LSF
            fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(20,5), sharex=True, sharey=False)
            ax1.step(wl, fl, color="0.4", lw=2, label="original")
            ax1.step(wl_uni, fl_uni, color="tab:red", lw=1, label="uniform sampling")
            ax1.legend(loc=2, frameon=False)
            ax2.step(wl_uni, fl_uni / fl_norm, color="tab:red", lw=2, label="uniform sampling")
            ax2.step(wl_new, fl_new, color="tab:blue", lw=1, label="matched LSF")
            ax2.legend(loc=2, frameon=False)
            if args.plot == 1:
                plt.show()
            elif args.plot == 2:
                fig.savefig(os.path.join(args.output_path, f"{sed_name}.png"), bbox_inches="tight")
                plt.close(fig)

        header_new = sed_fits[0].header.copy()
        del header_new["*COMMENT*"]
        header_new["FNORM"] = (fl_norm, "[erg/cm^2/s/AA] normalization flux")
        header_new.add_comment("", before="FNORM")
        header_new.add_comment("*** Pre-processing ***", before="FNORM")
        header_new.add_comment("", before="FNORM")
        hdu_0 = fits.PrimaryHDU(header=header_new)

        columns = [
            fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl_new),
            fits.Column(name=SPECTRA_TYPES[0], unit="FNORM", format="E", array=fl_new),
            fits.Column(name=SPECTRA_TYPES[1], unit="FNORM", format="E", array=fe_new),
            fits.Column(name=SPECTRA_TYPES[3], unit="AA", format="E", array=np.sqrt(wr_new**2+diff_lsf**2)/2.355)
        ]
        hdu_1 = fits.BinTableHDU.from_columns(fits.ColDefs(columns), name="SPECTRA")
        hdulist = fits.HDUList([hdu_0, hdu_1])
        hdulist.writeto(out_path, overwrite=True, output_verify="silentfix")
