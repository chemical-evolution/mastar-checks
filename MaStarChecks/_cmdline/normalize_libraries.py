
import sys, os
import argparse
import numpy as np
import pandas as pd
from tqdm import tqdm

from MaStarChecks.constants import HOMOGENEOUS_DATA_PATH, SPECTRA_TYPES, NAN_PLACEHOLDER
from MaStarChecks.stellarLibraries.catalogues import build_indous_catalogue, build_miles_catalogue, build_mastar_catalogue, build_gsl_catalogue, build_bosz_catalogue
from MaStarChecks.stellarLibraries.catalogues import extract_catalogue
from MaStarChecks.stellarLibraries.spectra import build_indous_library, build_miles_library, build_mastar_library, build_gsl_library, build_bosz_library
from MaStarChecks.stellarLibraries.spectra import extract_spectra

import warnings
from astropy.io.fits.verify import VerifyWarning


warnings.simplefilter('ignore', category=VerifyWarning)
__script__ = os.path.basename(__file__).replace(".py", "")
CWD = os.path.abspath(".")

INSTALLED_LIBRARIES = ["IndoUS", "MILES", "MaStar", "GSL", "BOSZ"]
CATALOGUE_BUILDERS = dict(zip(
    INSTALLED_LIBRARIES,
    [build_indous_catalogue, build_miles_catalogue, build_mastar_catalogue,
     build_gsl_catalogue, build_bosz_catalogue]
))
LIBRARY_BUILDERS = dict(zip(
    INSTALLED_LIBRARIES,
    [build_indous_library, build_miles_library, build_mastar_library,
     build_gsl_library, build_bosz_library]
))
COLUMNS_TO_EXTRACT = dict(zip(
    INSTALLED_LIBRARIES,
    [["FLUX"], ["FLUX"], SPECTRA_TYPES, ["FLUX"], ["FLUX"]]
))

def _main(cmd_args=sys.argv[1:]):
    parser = argparse.ArgumentParser(description="normalization of the stellar spectra libraries")
    parser.add_argument(
        "--output-path",
        help=f"path where to store normalized FITS. Defaults to '{HOMOGENEOUS_DATA_PATH}'",
        default=HOMOGENEOUS_DATA_PATH
    )
    parser.add_argument(
        "--libraries",
        help=f"which libraries to normalize. Defaults to all installed libraries: {', '.join(INSTALLED_LIBRARIES)}",
        nargs="+",
        default=INSTALLED_LIBRARIES,
        choices=INSTALLED_LIBRARIES
    )
    args = parser.parse_args(cmd_args)
    
    if not os.path.isdir(args.output_path): os.makedirs(args.output_path)

    for label in args.libraries:
        tqdm.write(f"\nnormalizing {label} library")
        catalogue = CATALOGUE_BUILDERS[label](use_cache=False).fillna(NAN_PLACEHOLDER)
        hdus = LIBRARY_BUILDERS[label](catalogue=catalogue, output_path=args.output_path, use_cache=False)

        catalogue = extract_catalogue(hdus, library_name=label)
        catalogue.to_csv(os.path.join(args.output_path,f"{label.lower()}-catalogue.csv"), index=False)
        del catalogue

        for type_ in COLUMNS_TO_EXTRACT[label]:
            seds = extract_spectra(hdus, column=type_, library_name=label)
            seds.to_csv(os.path.join(args.output_path,f"{label.lower()}-{type_.lower()}.csv"), index=False)
            del seds
        del hdus
