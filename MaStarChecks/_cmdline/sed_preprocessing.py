
import sys
import os
import argparse
from copy import deepcopy as copy

import numpy as np
import pandas as pd
from tqdm import tqdm
from astropy.io import fits

from MaStarChecks.constants import CARDS, NAN_PLACEHOLDER, BAR_FORMAT
from MaStarChecks.constants import SPECTRA_TYPES, ERROR_FRACTION, WAVELENGTH_NORM, TESTING_SIZE
from MaStarChecks.common import no_traceback
from MaStarChecks.io import load_libraries
from MaStarChecks.preprocessing import detect_outliers, fill_missing, downgrade_resolution


__script__ = os.path.basename(__file__).replace(".py", "")
CWD = os.path.abspath(".")


def filter_libraries(args, libraries, catalogues):
    # for each library:
    for label in args.labels:
        catalogue = catalogues[label]
        library = libraries[label]

        flag_columns = catalogue.columns[catalogue.columns.str.contains("GOOD")]

        #   replace NAN_PLACEHOLDER with True in GOOD* fields (spectra with no quality flags are assumed good)
        catalogue.loc[:, flag_columns] = catalogue.loc[:, flag_columns].replace({NAN_PLACEHOLDER: True})

        #   collapse GOOD* fields in the catalogues so that all criteria are met
        good_spectra = catalogue.loc[:, flag_columns].all(axis="columns")

        #   check for repeated stars & keep only those with good quality flags
        if catalogue is not None:
            catalogues[label] = catalogue.loc[good_spectra].reset_index().groupby("ID").first().reset_index().set_index("index")

            for type_, spectra in library.items():
                if spectra is not None:
                    libraries[label][type_] = spectra.loc[catalogues[label].index]

    #   return the resulting spectra dataframes and the catalogues
    return libraries, catalogues

def spectra_cleaning(args, libraries):
    # STEPS:
    # * mark outliers (each library)
    # * impute empties (each library)
    clean_libraries = {label: dict.fromkeys(SPECTRA_TYPES) for label in args.labels}
    for label in args.labels:
        library_mask = libraries[label]["FLUX"]<=0 if libraries[label]["MASK"] is None else libraries[label]["MASK"].astype(bool) | libraries[label]["FLUX"]<=0
        if args.detect_outliers and args.fill_missing:
            outliers_mask = detect_outliers(libraries[label]["FLUX"], mask=library_mask)
            clean_libraries[label]["FLUX"] = libraries[label]["FLUX"].mask(outliers_mask).dropna(how="all", axis="columns")
            clean_libraries[label]["FLUX"] = fill_missing(clean_libraries[label]["FLUX"])
        elif args.detect_outliers:
            outliers_mask = detect_outliers(libraries[label]["FLUX"], mask=library_mask)
            clean_libraries[label]["FLUX"] = libraries[label]["FLUX"].mask(outliers_mask).dropna(how="all", axis="columns")
        elif args.fill_missing:
            clean_libraries[label]["FLUX"] = fill_missing(libraries[label]["FLUX"], mask=library_mask)
        else:
            clean_libraries[label]["FLUX"] = libraries[label]["FLUX"].mask(library_mask)

        for type_ in args.ingredients:
            if type_ == "FLUX": continue
            if libraries[label].get(type_) is not None:
                clean_libraries[label][type_] = libraries[label][type_].reindex_like(clean_libraries[label]["FLUX"])
                if type_ == "ERROR": clean_libraries[label][type_].fillna(0.1*clean_libraries[label]["FLUX"])

    return clean_libraries

def get_spectra_sampling(args, libraries):

    # TODO: implement SIGINST column to deal with variable spectral resolution

    # compute libraries instrumental sigma
    library_sigmas = np.array(args.FWHM) / (2*np.sqrt(2*np.log(2)))
    # determine the maximum instrumental sigma among all options given in the command line
    max_sigma = np.max(library_sigmas)
    nominal_sigmas = {label: sigma for label, sigma in zip(args.labels,np.sqrt(max_sigma**2 - library_sigmas**2))}

    # determine best wavelength sampling
    if args.wavelength_sampling is not None:
        range, nominal_sampling = args.wavelength_sampling[:2], args.wavelength_sampling[2]
    else:
        wl_step, wl_min, wl_max = [], [], []
        for label in args.labels:
            old_wavelength = libraries[label]["FLUX"].columns.astype(float)
            wl_step.extend(np.unique(np.diff(old_wavelength)))
            wl_min.append(old_wavelength.min())
            wl_max.append(old_wavelength.max())

        range = np.ceil(np.max(wl_min)), np.floor(np.min(wl_max))
        nominal_sampling = np.ceil(np.max(wl_step))

    new_wavelength = np.arange(range[0], range[1]+nominal_sampling, nominal_sampling)

    return nominal_sigmas, new_wavelength

def pipeline(args, libraries, catalogues):
    # STEPS:
    # * determine best resolution (all libraries)
    # * determine best sampling (all libraries)
    # * downgrade resolution (each spectrum)
    # * resample (each spectrum)
    # * flux normalization (each spectrum)
    nominal_sigmas, new_wavelength = get_spectra_sampling(args, libraries)
    new_libraries = {label: dict.fromkeys([k for k, v in libraries[label].items() if v is not None and k != "MASK"]) for label in args.labels}
    new_catalogues = {label: catalogues[label].copy() for label in args.labels}
    for label in args.labels:
        spectra_types = {k: [] for k, v in libraries[label].items() if v is not None and k != "MASK"}
        iterator = tqdm(
            libraries[label]["FLUX"].index,
            desc=f"resampling {label}",
            unit="SED",
            ascii=True,
            dynamic_ncols=True,
            bar_format=BAR_FORMAT
        )
        for idx in iterator:
            for type_ in spectra_types:
                spectrum = libraries[label][type_].loc[idx]
                wavelength, y = spectrum.index.values.astype(float), spectrum.values

                if type_ in ["FLUX", "ERROR"]: y = downgrade_resolution(wavelength, spectrum=y, sigma=nominal_sigmas[label])

                # TODO: when extrapolating to zero, this will create spectra with invalid values (<= 0) by definition
                #       a good way to solve this would replace the args.extrapolate_const value to np.nan if <= 0.
                spectra_types[type_].append(pd.Series(
                    index=new_wavelength,
                    data=np.interp(
                        new_wavelength,
                        wavelength,
                        y,
                        left=args.extrapolate_const,
                        right=args.extrapolate_const
                    )
                ))

            # if libraries[label].get("ERROR") is not None:
            #     fe = libraries[label]["ERROR"].loc[idx].values
            #     fe = downgrade_resolution(wavelength=wl, flux=fe, sigma=nominal_sigmas[label])
            #     spectra_types["ERROR"].append(pd.Series(index=new_wavelength, data=np.interp(new_wavelength,wl,fe,left=args.extrapolate_const,right=args.extrapolate_const)))

        new_libraries[label] = {type_: pd.DataFrame(index=libraries[label][type_].index, data=matrix) if matrix else None for type_, matrix in spectra_types.items()}

        wl_norm = new_wavelength[(new_wavelength>=args.wavelength_norm[0])&(new_wavelength<=args.wavelength_norm[1])]
        fl_norm = new_libraries[label]["FLUX"].filter(items=wl_norm).agg("median", axis="columns")

        new_libraries[label]["FLUX"] = new_libraries[label]["FLUX"] / fl_norm.values[:, None]
        if libraries[label].get("ERROR") is not None:
            new_libraries[label]["ERROR"] = (new_libraries[label]["ERROR"]) / fl_norm.values[:, None]

        new_catalogues[label]["FNORM"] = fl_norm
        if libraries[label].get("ERROR") is not None:
            new_catalogues[label]["SNR"] = np.nanmedian(new_libraries[label]["FLUX"] / new_libraries[label]["ERROR"], axis=1)

    return new_libraries, new_catalogues

def write_fits(args, libraries, catalogues):
    if not os.path.isdir(args.output_path): os.mkdir(args.output_path)
    for label in args.labels:
        library, catalogue = libraries[label], catalogues[label].fillna(NAN_PLACEHOLDER)
        for idx, params in catalogue.iterrows():
            fits_name = f"processed-{label.lower()}-{params.ID}.fits.gz" if params.RECORD == NAN_PLACEHOLDER else f"processed-{label.lower()}-{params.ID}-{params.RECORD}.fits.gz"
            dest_name = os.path.join(args.output_path, fits_name)

            hdr = fits.Header()
            for (name, comment) in CARDS:
                value = params.get(name, NAN_PLACEHOLDER)
                hdr.append((name, value.strip() if isinstance(value,str) else value, comment))

            columns = []
            columns.append(fits.Column(name="WAVELENGTH", unit="AA", format="E", array=library["FLUX"].columns))
            columns.append(fits.Column(name="FLUX", unit="erg/s/cm^2/AA", format="E", array=library["FLUX"].loc[idx]))
            if library.get("ERROR") is not None:
                columns.append(fits.Column(name="ERROR", unit="erg/s/cm^2/AA", format="E", array=library["ERROR"].loc[idx]))
            if library.get("SIGINST") is not None:
                columns.append(fits.Column(name="SIGINST", unit="AA", format="E", array=library["SIGINST"].loc[idx]))

            primary_hdu = fits.PrimaryHDU(header=hdr)
            table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs(columns), name="SPECTRA")
            hdulist = fits.HDUList([primary_hdu, table_hdu])
            hdulist.writeto(dest_name, overwrite=True)

    return None

# TODO: define the pipeline as a decorated function, where the only parameter is the stellar
#       library dataframe and the additional arguments (kwargs) will be switches for the steps
#       in the pipeline. Define the decorator as a class as in https://bit.ly/3g3QbNG

# TODO: some steps of the pre-processing will be outside the pipeline, such as the calculation
#       of the nominal resolution and the sampling steps as they depend on all the libraries
#       simultaneously
def _main(cmd_args=sys.argv[1:]):
    additional_types = copy(SPECTRA_TYPES)
    additional_types.remove("FLUX")

    parser = argparse.ArgumentParser(description="pre-processing of stellar spectra libraries")
    parser.add_argument(
        "labels", metavar="library-labels",
        help="the library labels to look for when loading the stellar spectra libraries",
        nargs="+"
    )
    parser.add_argument(
        "--FWHM", metavar="fwhm",
        help="instrumental FWHMs used to compute a nominal spectral resolution. This parameter accepts several values at once",
        nargs="*",
        type=np.float,
        required=True
    )
    parser.add_argument(
        "--wavelength-sampling", metavar=("wl_ini", "wl_fin", "wl_step"),
        help="wavelength sampling of the final spectra libraries",
        nargs=3,
        type=np.float
    )
    parser.add_argument(
        "--extrapolate-const",
        help="the value to use when extrapolating during resampling of the spectra. Defaults to the extrema values",
        type=np.float
    )
    parser.add_argument(
        "-f", "--fill-missing",
        help="fill the missing values from the libraries. By default all flux values <= 0 are flagged as missing",
        action="store_true"
    )
    parser.add_argument(
        "-o", "--detect-outliers",
        help="run an outlier detection algorithm. All outliers are flagged as missing along with <= 0 fluxes",
        action="store_true"
    )
    parser.add_argument(
        "-i", "--ingredients",
        help=f"additional ingredients to load for the pre-processing. By default tries to load all: {additional_types}.",
        nargs="*",
        default=additional_types
    )
    parser.add_argument(
        "--assume-error",
        help=f"on missing error values, this is the fraction of the flux that will be asummed to be the error at each wavelength. Defaults to {ERROR_FRACTION}",
        default=ERROR_FRACTION
    )
    parser.add_argument(
        "--wavelength-norm", metavar=("wl_norm_ini", "wl_norm_fin"),
        help=f"the normalization windows in wavelength. The processed fluxes (and errors) will be normalized using the median within this range. Defaults to {WAVELENGTH_NORM}",
        nargs=2,
        default=WAVELENGTH_NORM,
        type=np.float
    )
    parser.add_argument(
        "--input-path",
        help="path from where the input files will be loaded. Defaults to current working directory",
        default=os.path.abspath(CWD)
    )
    parser.add_argument(
        "--output-path",
        help="path from where to store processed FITS. Defaults to current working directory",
        default=os.path.abspath(CWD)
    )
    parser.add_argument(
        "-d", "--debug",
        help="run in debugging mode (meant to test changes in the code)",
        action="store_true"
    )
    parser.add_argument(
        "-t", "--testing",
        help="run in testing mode (meant to test the data sets)",
        action="store_true"
    )
    parser.add_argument(
        "-n", "--testing-size",
        help=f"when running in testing and/or debugging mode, the size of the sample spectra in each library. Defaults to {TESTING_SIZE}",
        default=TESTING_SIZE
    )
    args = parser.parse_args(cmd_args)

    if not args.debug:
        sys.excepthook = no_traceback

    if "FLUX" not in args.ingredients:
        args.ingredients = ["FLUX"]+args.ingredients
    if len(args.labels) > len(args.FWHM):
        raise ValueError(f"\n[{__script__}] you need to provide at least one FWHM per stellar library. Use -h to get help\n")
    if args.extrapolate_const <= 0.0:
        args.extrapolate_const = np.nan

    libraries, catalogues = load_libraries(
        labels=args.labels,
        spectra_types=args.ingredients,
        kind=None,
        libraries_path=args.input_path,
        use_cache=False
    )
    libraries, catalogues = filter_libraries(args, libraries, catalogues)
    libraries = spectra_cleaning(args=args, libraries=libraries)
    new_libraries, new_catalogues = pipeline(args=args, libraries=libraries, catalogues=catalogues)

    write_fits(args=args, libraries=new_libraries, catalogues=new_catalogues)
