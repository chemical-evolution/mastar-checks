
import os
import pickle
import numpy as np
import numba as nb


@nb.njit
def chisq_distance(u, v, su, sv):
    """Return the chi square distance between two spectra"""
    chi_sq = (u-v)**2 / (su**2+sv**2)
    chi_sq[np.isinf(chi_sq)] = 0.0
    chi_sq[np.isnan(chi_sq)] = 0.0
    return 1/(u.size-1-2) * np.sum(chi_sq)

@nb.njit
def pairwise_chisq(X, Xerr, n):
    """Return the pairwise chi square distance vector from given spectra and error matrices"""
    j, m = 0, X.shape[0]
    distances = [0.0 for i in range(n)]
    while j<m:
        i = 0
        while i<j:
            k = m * i + j - ((i + 2) * (i + 1)) // 2
            distances[k] = chisq_distance(X[i], X[j], Xerr[i], Xerr[j])
            i += 1
        j += 1

    return np.asarray(distances)
