
import numpy as np
import pandas as pd

from scipy.spatial.distance import squareform
from scipy.cluster import hierarchy
from sklearn import metrics


def trim_tree(data, distances, linkage, cut_nclusters):
    """Return the scores of the clusters at the given cuts
    in number of clusters of the linkage

    Parameters
    ----------
    data: array_like
        The n x m array of n observations & m features from which the
        distances and the linkage was constructed
    distances: array_like
        The combination of n in 2 array of distances between the
        observations in 'data'
    linkage: array_like
        The n x 4 linkage matrix from the scipy.cluster.linkage routine
    cut_nclusters: array_like
        The array of number of clusters at which the linkage matrix will
        be cutted

    Returns
    -------
    labels: array_like
        The n x len(cut_nclusters) array of labels for each cut
    scores: pd.DataFrame
        A dataframe with the scores:
            Silhouette,
            CH,
            DB,
            Cophenet,
            Inconsistent
    """

    labels = hierarchy.cut_tree(linkage, n_clusters=cut_nclusters)

    distances_mat = squareform(distances)
    nclus = np.zeros(labels.shape[1], dtype=int)
    score = np.zeros((labels.shape[1],5))
    for i, cut_labels in enumerate(labels.T):
        nclus[i] = len(set(cut_labels))

        score[i, 0] = metrics.silhouette_score(distances_mat, cut_labels, metric="precomputed")
        score[i, 1] = metrics.calinski_harabasz_score(data, cut_labels)
        score[i, 2] = metrics.davies_bouldin_score(data, cut_labels)
        score[i, 3] = hierarchy.cophenet(linkage, distances)[0]
        score[i, 4] = hierarchy.inconsistent(linkage)[:, -1].mean()

    score[:, 1] /= score[:, 1].max()

    scores = pd.DataFrame(
        index=list(nclus),
        columns=["Silhouette","CH","DB","Cophenet","Inconsistent"],
        data=score
    )

    return labels, scores

def set_labels(X, Z, max_distance=None, n_clusters=None):
    """Return the selected labels and the position of the cluster centers
    in a Euclidean sense"""
    if max_distance is not None:
        labels = hierarchy.fcluster(Z, criterion="distance", t=max_distance) - 1
    elif n_clusters is not None:
        labels = hierarchy.fcluster(Z, criterion="maxclust", t=n_clusters) - 1
    else:
        raise ValueError(f"[set_labels] you have to specify either 'min_distances' or 'n_clusters'")

    cluster_cent = []
    for i in np.unique(labels):
        cluster_mean = X[labels==i].mean(axis=0)
        cluster_cent.append(np.argmin(np.sqrt(((cluster_mean[None, :] - X)**2).sum(axis=1)), axis=0))

    return labels, np.asarray(cluster_cent)

def mask_clusters(labels, cluster_size_threshold):
    """Return the clusters to keep and the mask of labels that satisfy:

        mask = cluster size >= cluster_size_threshold

    such that X[mask] is the filtered spectra matrix
    """
    i_clusters, cluster_sizes = np.unique(labels, return_counts=True)
    clusters_to_keep = i_clusters[cluster_sizes>=cluster_size_threshold]

    return clusters_to_keep, np.isin(labels, clusters_to_keep)
