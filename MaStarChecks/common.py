
import numpy as np
from tqdm import tqdm


def has_any_bits(number, bits):
    number_bits = np.array(list(bin(number)[2:][::-1]), dtype=int)
    active_bits = np.nonzero(number_bits)[0]
    return np.any(np.isin(active_bits, bits))

def get_grid(s, size):
    """Return a regular grid of the given array
    """
    delta = np.max(np.diff(s))
    grid = np.linspace(s.min(), s.max(), size)
    return grid

def no_traceback(type, msg, traceback):
  tqdm.write(f"\n{msg}\n")
  return None
