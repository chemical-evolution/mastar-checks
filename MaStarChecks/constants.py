import os


# MISC ---------------------------------------------------------------------------------------------
# progress bar formatting for tqdm package
BAR_FORMAT = '{percentage:3.0f}%|{bar:50}| {desc}: {n_fmt}/{total_fmt} [{elapsed}<{remaining}, ' '{rate_fmt}{postfix}]'

# NaN filler
NAN_PLACEHOLDER = -999999

# cards in homogeneous libraries header
CARDS = [
    ("ID", "identifier of the object"),
    ("RECORD", "record in original spectra FITS"),
    ("BIBREF", "bibliographic reference"),
    ("NAMES", ""),
    ("PHOTCAT", "photometric catalogue source"),
    ("NEXP", "number of expositions/visits"),
    ("SCI", "is science or standard star"),
    ("RA", "[HH:MM:SS.ss]"),
    ("DEC", "[DD:MM:SS.ss]"),
    ("DIST", "[kpc] distance"),
    ("EBV", "[mag] B-V color excess"),
    ("VCORR", "normalized volume correction"),
    ("SNR", "median signal-to-noise across the wavelengths"),
    ("FNORM", "[erg/s/cm^2/AA] normalization flux"),
    ("BMAG", "[mag] B magnitude"),
    ("VMAG", "[mag] V magnitude"),
    ("USDSS", "[mag] u SDSS"),
    ("GSDSS", "[mag] g SDSS"),
    ("RSDSS", "[mag] r SDSS"),
    ("ISDSS", "[mag] i SDSS"),
    ("ZSDSS", "[mag] z SDSS"),
    ("SPTYPE", "spectral type"),
    ("RADVEL", "[km/s] radial velocity"),
    ("RVERR", "[km/s] radial velocity error"),
    ("GOODSP", "good quality spectrum"),
    ("GOODPH", "good quality photometry"),
    ("GOODRV", "good quality radial velocity"),
    ("TEFF", "[K] effective temperature"),
    ("LOGG", "[log/cm/s^2] surface gravity"),
    ("MET", "[Fe/H] metallicity"),
    ("ALPHAM", "[alpha/M] alpha-enhancement")
]
# types of spectra in homogeneous libraries table
SPECTRA_TYPES = ["FLUX", "ERROR", "MASK", "SIGINST"]

# fraction of the flux on missing errors that will be assumed as the error at a given wavelength
ERROR_FRACTION = 0.085

# normalization windows
WAVELENGTH_NORM = 5490, 5510

# testing sample size
TESTING_SIZE = 100

APOGEE_COLUMNS = ["APOGEE_ID", "RA", "DEC", "TEFF", "LOGG", "M_H", "ALPHA_M",
                  "TEFF_ERR", "LOGG_ERR", "M_H_ERR", "ALPHA_M_ERR"]

# MaStar bits to flag
BITS2FLAG = [1,2,3,4,5,6,7,8,9,11,12,13,14,15,16]

# Gaia zero points
GZP, BPZP, RPZP = [2.48852e-9, 4.04876e-9, 1.2945e-9]

# PHYSICAL CONSTANTS -------------------------------------------------------------------------------

R_V = 3.1

# PATHS --------------------------------------------------------------------------------------------

ROOT_PATH = os.path.dirname(os.path.dirname(__file__))
STORE_PATH = "/disk-a/mejia"
DATA_PATH = os.path.join(STORE_PATH, "mastar-checks-data")
CACHE_PATH = os.path.join(DATA_PATH, "cache")

STELLAR_LIBRARIES_PATH = os.path.join(DATA_PATH, "stellar-libraries")
HOMOGENEOUS_DATA_PATH = os.path.join(os.path.expandvars("$astro/mastar-checks/notebooks/fitting/_data"), "homogeneous")

INDOUS_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "IndoUS")
INDOUS_CATALOGUE_PATH = os.path.join(INDOUS_PATH, "cflibdb.fits")
INDOUS_SEDS_PATH = os.path.join(INDOUS_PATH, "indous_seds")

MILES_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "MILES")
MILES_REFERENCE_PATH = os.path.join(MILES_PATH, "MILES-reference.txt")
MILES_CATALOGUE_PATH = os.path.join(MILES_PATH, "MILES-catalog.txt")
MILES_PARAMETER_PATH = os.path.join(MILES_PATH, "MILES-params.txt")
# MILES_SEDS_PATH = os.path.join(MILES_PATH, "miles_seds")

XSHOOTER_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "X-shooter")
XSHOOTER_REFERENCE_PATH = os.path.join(XSHOOTER_PATH, "tablea3.dat")
XSHOOTER_CATALOGUE_PATH = os.path.join(XSHOOTER_PATH, "tablea2.dat")
XSHOOTER_PARAMETER_PATH = os.path.join(XSHOOTER_PATH, "tablea1.dat")
XSHOOTER_SEDS_PATH = os.path.join(XSHOOTER_PATH, "x-shooter_seds")

MASTAR_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "MaStar")
MASTAR_CATALOGUE_PATH = os.path.join(MASTAR_PATH, "mastarall-v3_1_1-v1_7_7.fits")
MASTAR_SEDS_PATH = os.path.join(MASTAR_PATH, "mastar-goodspec-v3_1_1-v1_7_7-lsfpercent99.5.fits")
MASTAR_GAIA_PATH = os.path.join(MASTAR_PATH, "mastarall-gaia-v3_1_1-v1_7_7.fits")
MASTAR_GAIA_EBV_PATH = os.path.join(MASTAR_PATH, "mastarall-gaiaedr3-ebv-v3_1_1-v1_7_7.fits")

MWM_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "MWM")
MWM_CATALOGUE_PATH = os.path.join(MWM_PATH, "spAll-v6_0_4.fits")
MWM_SEDS_PATH = os.path.join(MWM_PATH, "spectra", "full")

GSL_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "GSL")
GSL_SEDS_PATH = os.path.join(GSL_PATH, "MedResFITS/A1FITS")
GSL_WAVE_PATH = os.path.join(GSL_PATH, "HiResFITS/WAVE_PHOENIX-ACES-AGSS-COND-2011.fits")

BOSZ_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "BOSZ")
BOSZ_SEDS_PATH = os.path.join(BOSZ_PATH, "bosz_seds")

GAIA_PATH = os.path.join(STELLAR_LIBRARIES_PATH, "Gaia")
GAIA_GBAND_PATH = os.path.join(GAIA_PATH, "GAIA_GAIA2.G.dat")
GAIA_BPBAND_PATH = os.path.join(GAIA_PATH, "GAIA_GAIA2.Gbp.dat")
GAIA_RPBAND_PATH = os.path.join(GAIA_PATH, "GAIA_GAIA2.Grp.dat")
# --------------------------------------------------------------------------------------------------
# INSTRUMENTAL CONSTANTS ---------------------------------------------------------------------------

INDOUS_FWHM = 1.0
MILES_FWHM = 2.5
XSHOOTER_FWHM = None
GSL_FWHM = 1.0
BOSZ_FWHM = 0.7

# the resolution of the MANGA spectroscopy varies at different wavelengths as:
# delta lambda = lambda/R ~ 3600/1370, 6000/2280, 6000/1780, 10500/2990
# which gives 2.63 angstroms as minimum value
MASTAR_FHWM = 2.63
