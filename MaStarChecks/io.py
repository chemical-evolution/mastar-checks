
import os
import pickle
import itertools as it
from copy import deepcopy as copy

import numpy as np
from tqdm import tqdm
import pandas as pd
from astropy.io import fits
from astropy.table import Table

from MaStarChecks.constants import NAN_PLACEHOLDER, BAR_FORMAT
from MaStarChecks.constants import APOGEE_COLUMNS
from MaStarChecks.stats import gaussian_kde
from MaStarChecks.stellarLibraries.catalogues import extract_catalogue
from MaStarChecks.stellarLibraries.spectra import extract_spectra
from MaStarChecks.preprocessing import fill_missing


def load_libraries(labels, spectra_types=["FLUX"], kind="processed", libraries_path=".", use_cache=True, cache_path=".", cache_label=None):
    """Load the libraries given their labels, with the possibility of caching in a pickle file
    """
    labels_ = list(map(str.lower, labels))
    if cache_label is None:
        cache_file = os.path.join(cache_path, "features-{}.p".format("-".join(labels_)))
    else:
        cache_file = os.path.join(cache_path, "features-{}-{}.p".format("-".join(labels_),cache_label))

    if use_cache and os.path.isfile(cache_file):
        spectra, catalogues = pickle.load(open(cache_file,"rb"))
    else:
        catalogues = dict.fromkeys(labels)
        spectra = {label: dict.fromkeys(spectra_types) for label in labels}
        for i, label in enumerate(labels_):
            if kind:
                fits_list = sorted([os.path.join(libraries_path,fitsname) for fitsname in os.listdir(libraries_path) if fitsname.startswith(f"{kind}-{label}-") and fitsname.endswith("fits.gz")])
            else:
                fits_list = sorted([os.path.join(libraries_path,fitsname) for fitsname in os.listdir(libraries_path) if fitsname.startswith(f"{label}-") and fitsname.endswith("fits.gz")])
            if len(fits_list) == 0:
                raise ValueError(f"[load_libraries] no library found with the label '{label}'")

            hdus = []
            for fitspath in tqdm(fits_list, desc=f"reading FITS for {labels[i]}", unit="FITS", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
                with fits.open(fitspath, memmap=False) as hdu:
                    hdus.append(copy(hdu))

            catalogue = extract_catalogue(hdus, labels[i])
            catalogue = catalogue.apply(lambda s: s.mask(s==NAN_PLACEHOLDER) if s.dtype!="O" else s.mask(s==str(NAN_PLACEHOLDER)))

            catalogues[labels[i]] = catalogue
            for spectra_type in spectra_types:
                spectra[labels[i]][spectra_type] = extract_spectra(hdus, labels[i], column=spectra_type)

        pickle.dump((spectra,catalogues), open(cache_file,"wb"))

    return spectra, catalogues

def load_apogee(distances_catalogue, aspcap_catalogue, cannon_catalogue, columns=APOGEE_COLUMNS):

    apogee_distances = pd.read_csv(distances_catalogue, index_col="APOGEE_ID").filter(items=["DIST"])
    apogee_distances = apogee_distances.loc[~apogee_distances.index.duplicated()]

    apogee_fits = fits.open(aspcap_catalogue)
    catalogue_aspcap = Table(apogee_fits[1].data)[columns].to_pandas().set_index("APOGEE_ID")
    catalogue_aspcap = catalogue_aspcap.loc[~catalogue_aspcap.index.duplicated()]
    catalogue_aspcap = catalogue_aspcap.mask(catalogue_aspcap==-9999)
    catalogue_aspcap[["RA","DEC"]] = catalogue_aspcap[["RA","DEC"]].mask((catalogue_aspcap[["RA","DEC"]]==0).any(axis="columns"))
    catalogue_aspcap = pd.merge(left=catalogue_aspcap, right=apogee_distances, how="left", left_index=True, right_index=True)
    catalogue_aspcap = catalogue_aspcap.dropna(how="any")

    apogee_fits = fits.open(cannon_catalogue)
    catalogue_cannon = Table(apogee_fits[1].data).to_pandas().set_index("APOGEE_ID")
    catalogue_cannon = catalogue_cannon.loc[~catalogue_cannon.index.duplicated()]
    catalogue_cannon = pd.merge(left=catalogue_aspcap[["RA","DEC"]], right=catalogue_cannon, left_index=True, right_index=True)
    catalogue_cannon = catalogue_cannon.filter(items=columns)
    catalogue_cannon = pd.merge(left=catalogue_cannon, right=apogee_distances, how="left", left_index=True, right_index=True)
    catalogue_cannon = catalogue_cannon.mask(catalogue_cannon==-9999)
    catalogue_cannon = catalogue_cannon.reindex(index=catalogue_aspcap.index)

    return catalogue_aspcap, catalogue_cannon

def write_basis_parameters(params, errors, params_labels, columns, units, weights=None, limits=None, steps=None, label=None, path="."):
    """Write the clustered stellar parameters PDFs to a FITS file"""

    cluster_ids = np.unique(params_labels)
    hdu_imaxes = list(it.combinations(columns, 2))
    hdu_idaxes = list(it.combinations(range(len(columns)), 2))
    if steps is None:
        steps_ = 1/3 * np.median(errors, axis=0)
    elif np.isscalar(steps):
        steps_ = [steps] * params.columns.size
    else:
        steps_ = steps

    header = fits.Header([
        ("NCLUSTER", len(cluster_ids), "Number of clusters"),
        ("COMMENT", "The number of clusters refers to the minimum number of stellar spectra"),
        ("COMMENT", "that are distinguishable from one another")
    ])
    primary_hdu = fits.PrimaryHDU(header=header)

    hdu_list = fits.HDUList([primary_hdu])
    for (i, j), (name_x, name_y) in zip(hdu_idaxes, hdu_imaxes):
        unit_x, unit_y = units[i], units[j]
        if limits is None:
            x_lims, y_lims = (params[:,i].min(),params[:,i].max()), (params[:,j].min(),params[:,j].max())
        else:
            x_lims, y_lims = limits[i], limits[j]

        x_grid, y_grid = np.arange(x_lims[0],x_lims[1]+steps_[i],steps_[i]), np.arange(y_lims[0],y_lims[1]+steps_[j],steps_[j])
        X, Y = np.meshgrid(x_grid, y_grid)
        support = np.dstack((X.ravel(),Y.ravel()))

        pdf_cube = []
        for icluster in cluster_ids:
            params_icluster = params[params_labels==icluster]
            errors_icluster = errors[params_labels==icluster]
            weights_icluster = weights[params_labels==icluster]
            x_, y_ = params_icluster[:, i], params_icluster[:, j]
            x_err, y_err = errors_icluster[:, i], errors_icluster[:, j]

            if weights is not None:
                Z = gaussian_kde(
                    support=support,
                    mean=np.atleast_2d(np.column_stack((x_,y_))), sdev=np.atleast_2d(np.column_stack((x_err,y_err))),
                    kernel_weights=weights_icluster, h=1.0, return_kernels=False
                )
            else:
                Z = gaussian_kde(
                    support=support,
                    mean=np.atleast_2d(np.column_stack((x_,y_))), sdev=np.atleast_2d(np.column_stack((x_err,y_err))),
                    kernel_weights=None, h=1.0, return_kernels=False
                )
            Z = Z.reshape(X.shape)
            norm = np.sum(np.sum(Z * np.diff(X.ravel())[0], axis=0) * np.diff(Y.T.ravel())[0], axis=0)
            Z = Z / (norm if norm!=0.0 else 1.0)

            pdf_cube.append(Z.T)

        # TODO: cluster_ids cannot be an axis because some IDs can be missing after removing clusters
        #       with very few samples. Use a table extension instead to store the cluster IDs
        ext_hdu = fits.ImageHDU(
            data=np.dstack(pdf_cube).T,
            header=fits.Header([
                ("CRVAL1", x_lims[0]),
                ("CDELT1", steps_[i]),
                ("CRPIX1", 1),
                ("CTYPE1", name_x),
                ("CUNIT1", unit_x),
                ("CRVAL2", y_lims[0]),
                ("CDELT2", steps_[j]),
                ("CRPIX2", 1),
                ("CTYPE2", name_y),
                ("CUNIT2", unit_y),
                ("CRVAL3", 0),
                ("CDELT3", 1),
                ("CRPIX3", 1),
                ("CTYPE3", "CLUSTER ID"),
                ("CUNIT3", "")
            ])
        )
        hdu_list.append(ext_hdu)

    basis_name = f"stellar-basis-params-{label}.fits.gz" if label is not None else "stellar-basis-params.fits.gz"
    hdu_list.writeto(os.path.join(path, basis_name), overwrite=True)

    return hdu_list

def write_basis_seds(seds, wavelengths, norm_wlength, basis_params, columns, units, label=None, path="."):
    """Write the stellar basis spectra to a FITS file"""
    delt = np.diff(wavelengths)[0]

    hdu = fits.PrimaryHDU(
        data=seds,
        header=fits.Header([
            ("CRVAL1", wavelengths[0]),
            ("CDELT1", delt),
            ("CRPIX1", 1),
            ("CTYPE1", "wavelength"),
            ("CUNIT1", "AA"),
            ("CTYPE2", "flux"),
            ("CUNIT2", ""),
            ("WAVENORM", norm_wlength)
        ])
    )

    fmts = {column: "E" for column in columns}
    units_ = dict(zip(columns,units))
    columns_ = []
    for j, column in enumerate(columns):
        columns_.append(fits.Column(name=column, array=basis_params[:,j], format=fmts[column], unit=units_[column]))

    table_hdu = fits.BinTableHDU.from_columns(columns_)
    table_hdu.name = "PARAMETERS"

    hdu_list = fits.HDUList([hdu,table_hdu])

    basis_name = f"stellar-basis-spectra-{label}.fits.gz" if label is not None else "stellar-basis-spectra.fits.gz"
    hdu_list.writeto(os.path.join(path, basis_name), overwrite=True)
    return hdu_list

def write_fit3d_basis(seds, wavelengths, norm_wlength, basis_params, columns, units, solar_feh=0.019, label=None, path="."):
    """Write the stellar basis spectra to a FITS file in the FIT3D format"""
    delt = np.diff(wavelengths)[0]

    header_names, header_norms = [], []
    for i, row in enumerate(basis_params.round(4)):
        header_names.append((f"NAME{i}",
                             f"spec_ssp_{row[0]}_{row[1]}_{str(np.round(10 ** row[2] * solar_feh, 4)).replace('0.', 'z')}.dat"))
        header_norms.append((f"NORM{i}", row[4]))

    hdu = fits.PrimaryHDU(
        data=seds,
        header=fits.Header([
           ("CRVAL1", wavelengths[0]),
           ("CDELT1", delt),
           ("CRPIX1", 1),
           ("CTYPE1", "wavelength"),
           ("CUNIT1", "AA"),
           ("CTYPE2", "flux"),
           ("CUNIT2", "")
       ] + header_names + header_norms + [("WAVENORM", norm_wlength)])
    )
    hdu_list = fits.HDUList(hdu)

    basis_name = f"stellar-basis-spectra-{label}-fit3d.fits" if label is not None else "stellar-basis-spectra-fit3d.fits"
    hdu_list.writeto(os.path.join(path, basis_name), overwrite=True)
    return hdu_list
