
import numpy as np
import pandas as pd
import matplotlib.tri as tri
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable
import scipy.optimize as so
from scipy.stats import gaussian_kde
from scipy.interpolate import Rbf
import seaborn as sns


from MaStarChecks.common import get_grid
from .styles import MASTAR_COLOR, LIGHT_COLOR, QUARTILE_PALETTE


def density_scatter_hist(x, y, bins, **kwargs):
    """Return the KDE in the plane sampled by x and y

    inspired by: https://bit.ly/2TAwAuS
                 https://bit.ly/2zuSdWP

    Parameters
    ----------
    x, y: array_like
        The samples from which to compute the densities
    bins: same as np.histogram2d
    kwargs:
        Arguments passed to the interpolator scipy.interpolate.Rbf

    Returns
    -------
    pdf_z: function
        The estimated PDF function. The calling signature is
        pdf_z(x, y)
    """

    Z, x_e, y_e = np.histogram2d(x, y, bins=bins, density=True)
    X, Y = np.meshgrid(0.5*(x_e[1:] + x_e[:-1]), 0.5*(y_e[1:]+y_e[:-1]))
    pdf_z = Rbf(X, Y, Z.T, **kwargs)

    return pdf_z

def density_scatter_kde(x, y, **kwargs):
    """Returns the KDE in the plane sampled by x and y

    inspired by: https://bit.ly/2TAwAuS

    Parameters
    ----------
    x, y: array_like
        The samples from which to compute the densities
    kwargs:
        Arguments passed to the interpolator scipy.stats.gaussian_kde

    Returns
    -------
    pdf_z: function
        The estimated PDF function. The calling signature is
        pdf_z(x, y)
    """

    xy = np.vstack([x,y])
    pdf_z = gaussian_kde(xy, **kwargs)

    return lambda xex, yex: pdf_z.pdf(np.vstack([xex,yex]))

def contour_masks(Z, levels):
    """Return the masks of Z within the given levels.

    If Z is the evaluated PDF(x, y), these masks represent
    the points in the sample above the confidence level.
    If Z is the percentage distribution, these masks represent
    the bins in the distribution that add up to the given
    percentage levels.
    """

    levels_ = sorted(levels)

    masks = []
    for i in range(len(levels_)):
        masks.append(Z>levels_[i])

    return np.asarray(masks)

def find_confidence_interval(x, pdf, confidence_level):
    """Return the difference between the integrated PDF
    ABOVE a given threshold, and a target confidence level.

    Inspired by https://bit.ly/2A63f4A

    Parameters
    ----------
    x: float
        A threshold above which the PDF will be integrated
    percent_dist: array_like
        A PDF array
    percent: float
        Target confidence interval

    Returns
    -------
    residual: float
        integral(PDF > x) - confidence_level
    """
    return pdf[pdf > x].sum() - confidence_level

def contours_from_pdf(pdf_func, range_x, range_y, deltas=0.1, percentiles=[68,95,99], return_grid=False):
    """Return the contour levels that (nearly) represent the given
    confidence interval of the PDF.

    Inspired by: https://bit.ly/2A63f4A

    Parameters
    ----------
    pdf_func: function
        The PDF function from which to draw the confidence
        intervals
    range_y, range_y: tuple
        The ranges within which calculate the support of the
        PDF.
    deltas: float, tuple
        The step of the grid If float, the step will
        be the same in x and y. If tuple, the steps
        in x and the y directions, respectively
    percentiles: tuple
        The percentiles at which to compute the levels
    return_grid: boolean
        Whether to return also the grid X, Y, Z to draw the
        contours. Defaults to False

    Returns
    -------
    levels: array_like
        The sorted array of levels
    X, Y, Z: array_like, optional
        The arrays to draw the contours as in

        >>> plt.contour(X, Y, Z, levels=levels)
    """

    if not isinstance(deltas, str) and hasattr(deltas, "__getitem__"):
        delta_x, delta_y = deltas[:2]
    elif isinstance(deltas, (float, int)):
        delta_x, delta_y = deltas, deltas

    x_grid = np.arange(*range_x, delta_x)
    y_grid = np.arange(*range_y, delta_y)
    X, Y = np.meshgrid(x_grid, y_grid)

    Z = pdf_func(X.ravel(), Y.ravel())
    Z = Z.reshape(X.shape)

    prob = Z*delta_x*delta_y
    # in some cases the PDF will not be normalized to 1.0, fix this here...
    prob /= prob.sum()

    percentiles_ = np.asarray(sorted(percentiles, reverse=True))/100
    levels = np.asarray([so.brenth(find_confidence_interval, 0.0, prob.max(), args=(prob,p)) for p in percentiles_])
    levels = levels/delta_x/delta_y

    if return_grid:
        return np.asarray(levels), X, Y, Z

    return np.asarray(levels)

def find_percent_interval(x, percent_dist, percent):
    """Return the difference between the integrated percentage
    distribution ABOVE a given threshold, and a target
    percentage.

    Inspired by https://bit.ly/2A63f4A

    Parameters
    ----------
    x: float
        A threshold above which the percentage distribution
        will be integrated
    percent_dist: array_like
        A percentage distribution array
    percent: float
        Target integrated percentage

    Returns
    -------
    residual: float
        sum(percent_dist > x) - percent
    """
    return (percent_dist > x).sum() - percent

def contours_from_hist(x, y, range_x=None, range_y=None, deltas=0.1, percents=[68,95,99], return_grid=False):
    """Return the contour levels that (nearly) enclose the given
    percentages of the samples.

    Inspired by: https://bit.ly/2A63f4A

    Parameters
    ----------
    x, y: array_like
        The samples from which to drawn contours
    range_y, range_y: tuple
        The ranges within which calculate the histogram.
        If not given, use the limits of the samples
    deltas: float, tuple
        The width of the bins. If float, the width will
        be the same in x and y. If tuple, the bin widths
        in x and the y directions, respectively
    percents: tuple
        The percentages from which to draw
    return_grid: boolean
        Whether to return also the grid X, Y, Z to draw the
        contours. Defaults to False

    Returns
    -------
    levels: array_like
        The sorted array of levels
    X, Y, Z: array_like, optional
        The arrays to draw the contours as in

        >>> plt.contour(X, Y, Z, levels=levels)
    """

    if not isinstance(deltas,str) and hasattr(deltas,"__getitem__"):
        delta_x, delta_y = deltas[:2]
    elif isinstance(deltas, (float,int)):
        delta_x, delta_y = deltas, deltas

    if range_x is None:
        range_x = x.min(), x.max()
    if range_y is None:
        range_y = y.min(), y.max()

    x_grid = np.arange(*range_x, delta_x)
    y_grid = np.arange(*range_y, delta_y)
    H, x_grid, y_grid = np.histogram2d(x, y, bins=(x_grid,y_grid), density=False)

    X, Y = np.meshgrid((x_grid[1:]+x_grid[:-1])*0.5, (y_grid[1:]+y_grid[:-1])*0.5)
    per = H/H.sum()*100

    percents_ = np.asarray(sorted(percents, reverse=True))
    levels = []
    for p in percents_:
        xmin = so.brenth(find_percent_interval, 0.0, per.max(), args=(per,p))
        levels.append(xmin)

    if return_grid:
        return np.asarray(levels), X, Y, per.T

    return np.asarray(levels)

def triang_plot(df, labels, z=None, true=None, ngrid=20):
    fig, axs = plt.subplots(len(labels), len(labels), figsize=(12,12), sharex="col", sharey=False)
    fig.tight_layout()

    for i,j in zip(*np.triu_indices_from(axs, k=1)):
        plt.setp(axs[i,j], visible=False)

    for i,j in zip(*np.tril_indices_from(axs, k=0)):
        x, y = df[labels[j]], df[labels[i]]
        x_true, y_true = true[labels[j]], true[labels[i]]

        xe = get_grid(x, ngrid+1)
        ye = get_grid(y, ngrid+1)
        xi = 0.5 * (xe[1:] + xe[:-1])
        yi = 0.5 * (ye[1:] + ye[:-1])
        Xi, Yi = np.meshgrid(xi, yi, indexing="ij")

        if i==j:
            axs[i,j].hist(x, bins=xe, color="k", density=True)
            axs[i,j].axvline(x_true, ls="-", color="m", lw=3)
        else:
            if z is not None:
                triang = tri.Triangulation(x, y)
                interpolator = tri.LinearTriInterpolator(triang, z)
                Zi = interpolator(Xi, Yi)
            else:
                Zi, _ = np.histogramdd((x,y), bins=(xe,ye), density=True)

            Zi = Zi - Zi.min()
            Zi = Zi / Zi.max()
            axs[i,j].scatter(Xi, Yi, s=Zi * 100, c=np.sqrt((x_true-Xi)**2 + (y_true-Yi)**2), lw=0, cmap=plt.cm.plasma)

            axs[i,j].plot(x_true, y_true, "xm", mew=2)

            if axs[i,j].is_last_row():
                axs[i,j].set_xlabel(labels[j])
            if axs[i,j].is_first_col():
                axs[i,j].set_ylabel(labels[i])

    return fig

def move_legend(ax, new_loc, **kws):
    old_legend = ax.legend_
    handles = old_legend.legendHandles
    labels = [t.get_text() for t in old_legend.get_texts()]
    title = old_legend.get_title().get_text()
    ax.legend(handles, labels, loc=new_loc, title=title, **kws)

def build_comparison_table(tablea, tableb, labela, labelb, columns, delta_prefix=r"$\Delta$"):
    tablea_ = tablea.filter(items=columns)
    tableb_ = tableb.filter(items=columns)

    comparison = pd.merge(tablea_.add_suffix(f" {labela}"), tableb_.add_suffix(f" {labelb}"), left_index=True, right_index=True, how="inner")
    residuals = comparison.filter(like=labela).rename(columns=lambda s: s.replace(f" {labela}",""))-comparison.filter(like=labelb).rename(columns=lambda s: s.replace(f" {labelb}",""))
    residuals = residuals.add_prefix(delta_prefix)

    comparison = pd.concat((comparison,residuals), axis="columns")
    return comparison

def consistency_plot(comparison_table, column, unit, is_logscale, labelx, labely, lims=None, filled_levels=(0.25,0.50,0.75,1.00), dashed_levels=(0.05,), filled_palette=QUARTILE_PALETTE, dashed_color=LIGHT_COLOR, guide_color="w", margins_color=MASTAR_COLOR):
    # y = x + e*|x|
    # log(y) = log(x + e*|x|)
    # ---
    # log(y) - log(x) = log(y/x) = log(e)
    # log(y) = log(x) + log(e)

    # e = |(y - x)/x|
    # e*|x| = y - x
    # e*|x| +/- x = y
    # ---
    # e = 10^log(y/x) - 1
    # log(e + 1) = log(y/x)
    # log(y) - log(x) = log(e + 1)
    # log(y) = log(x) + log(e + 1)
    # log(y) = log(x * (e+1))

    summary = comparison_table.describe(percentiles=(0.01,0.99))

    if lims is None:
        rangea = summary.loc[["1%","99%"],f"{column} {labelx}"].values
        rangeb = summary.loc[["1%","99%"],f"{column} {labely}"].values
        xrange = np.array([min(*rangea, *rangeb), max(*rangea, *rangeb)])
    else:
        xrange = lims[:2]
    mu_a, sigma_a = summary.loc[["mean","std"],f"{column} {labelx}"].values
    mu_b, sigma_b = summary.loc[["mean","std"],f"{column} {labely}"].values

    g = sns.jointplot(data=comparison_table, x=f"{column} {labelx}", y=f"{column} {labely}", kind="kde", dropna=True, height=7,
                      levels=filled_levels, marginal_kws=dict(alpha=1.0, color=margins_color),
                      joint_kws=dict(colors=filled_palette), fill=True, xlim=xrange, ylim=xrange)

    g.ax_joint.plot(xrange, xrange, "-", lw=1, color=guide_color)
    if is_logscale:
        xscale = np.log10(np.linspace(*(10**xrange)))
        g.ax_joint.plot(xscale, np.log10(10**xscale * 1.1), "--", lw=0.7, color=guide_color)
        g.ax_joint.plot(xscale, np.log10(10**xscale / 1.1), "--", lw=0.7, color=guide_color)
    else:
        g.ax_joint.plot(xrange, xrange+np.abs(0.1*xrange), "--", lw=0.7, color=guide_color)
        g.ax_joint.plot(xrange, xrange-np.abs(0.1*xrange), "--", lw=0.7, color=guide_color)

    g.ax_marg_x.axvline(mu_a, ls="-", lw=0.7, color=guide_color)
    g.ax_marg_x.axvline(mu_a-sigma_a, ls="--", lw=0.7, color=guide_color)
    g.ax_marg_x.axvline(mu_a+sigma_a, ls="--", lw=0.7, color=guide_color)
    g.ax_marg_y.axhline(mu_b, ls="-", lw=0.7, color=guide_color)
    g.ax_marg_y.axhline(mu_b-sigma_b, ls="--", lw=0.7, color=guide_color)
    g.ax_marg_y.axhline(mu_b+sigma_b, ls="--", lw=0.7, color=guide_color)

    g.ax_joint.text(0.95, 0.10, f"{labelx} $\mu,\sigma={mu_a:.2f},{sigma_a:.2f}\,${unit}", ha="right", size="small", transform=g.ax_joint.transAxes)
    g.ax_joint.text(0.95, 0.05, f"{labely} $\mu,\sigma={mu_b:.2f},{sigma_b:.2f}\,${unit}", ha="right", size="small", transform=g.ax_joint.transAxes)

    sns.kdeplot(data=comparison_table, x=f"{column} {labelx}", y=f"{column} {labely}", levels=dashed_levels, color=dashed_color, linestyles="--", linewidths=1.5, ax=g.ax_joint)

    return g

def delta_plot(comparison_table, column, unit, labelx, delta_prefix=r"$\Delta$", limx=None, limy=None, filled_levels=(0.25,0.50,0.75,1.00), dashed_levels=(0.05,), filled_palette=QUARTILE_PALETTE, dashed_color=LIGHT_COLOR, guide_color="w", margins_color=MASTAR_COLOR):
    summary = comparison_table.describe(percentiles=(0.01,0.99))

#     xrange = summary.loc[["1%","99%"],f"{column} {labelx}"].values
    if limx is None:
        rangea = summary.loc[["1%","99%"],f"{column} {labelx}"].values
        rangeb = summary.loc[["1%","99%"],f"{delta_prefix}{column}"].values
        xrange = np.array([min(*rangea, *rangeb), max(*rangea, *rangeb)])
    else:
        xrange = limx[:2]
    if limy is None:
        yrange = summary.loc[["1%","99%"],f"{delta_prefix}{column}"].values
    else:
        yrange = limy[:2]
    yrange[1] *= 1.5
    mu, sigma = summary.loc[["mean","std"],f"{delta_prefix}{column}"].values

    g = sns.jointplot(data=comparison_table, x=f"{column} {labelx}", y=f"{delta_prefix}{column}", kind="kde", marginal_kws=dict(alpha=1.0, color=margins_color),
                      levels=filled_levels, joint_kws=dict(fill=True, colors=filled_palette), height=7, xlim=xrange, ylim=yrange)

    g.ax_joint.axhline(ls=":", lw=0.7, color=guide_color)
    g.ax_joint.axhline(mu, ls="-", lw=0.7, color=guide_color)
    g.ax_joint.axhline(mu-sigma, ls="--", lw=0.7, color=guide_color)
    g.ax_joint.axhline(mu+sigma, ls="--", lw=0.7, color=guide_color)

    g.ax_marg_y.axhline(ls=":", lw=0.7, color=guide_color)
    g.ax_marg_y.axhline(mu, ls="-", lw=0.7, color=guide_color)
    g.ax_marg_y.axhline(mu-sigma, ls="--", lw=0.7, color=guide_color)
    g.ax_marg_y.axhline(mu+sigma, ls="--", lw=0.7, color=guide_color)

    column_name = column.replace(f"~({unit})", "") if unit else column
    g.ax_joint.text(0.05, 0.05, f"{delta_prefix}{column_name}$\,={mu:.2f}\pm{sigma:.2f}\,${unit}", ha="left", size="small", transform=g.ax_joint.transAxes)

    sns.kdeplot(data=comparison_table, x=f"{column} {labelx}", y=f"{delta_prefix}{column}", levels=dashed_levels, color=dashed_color, linestyles="--", linewidths=1.5, ax=g.ax_joint)

    return g

def delta_inset_plot(comparison_table, column, unit, is_logscale, labelx, labely, delta_prefix=r"$\Delta$", limx=None, limy=None, filled_levels=(0.25,0.50,0.75,1.00), dashed_levels=(0.05,), filled_palette=QUARTILE_PALETTE, dashed_color=LIGHT_COLOR, guide_color="w", margins_color=MASTAR_COLOR):
    summary = comparison_table.describe(percentiles=(0.01,0.99))

#     xrange = summary.loc[["1%","99%"],f"{column} {labelx}"].values
    if limx is None:
        rangea = summary.loc[["1%","99%"],f"{column} {labelx}"].values
        rangeb = summary.loc[["1%","99%"],f"{column} {labely}"].values
        xrange = np.array([min(*rangea, *rangeb), max(*rangea, *rangeb)])
    else:
        xrange = limx[:2]
    if limy is None:
        yrange = summary.loc[["1%","99%"],f"{delta_prefix}{column}"].values
    else:
        yrange = limy[:2]
    yrange[1] *= 1.5
    mu_a, sigma_a = summary.loc[["mean","std"],f"{column} {labelx}"].values
    mu_b, sigma_b = summary.loc[["mean","std"],f"{column} {labely}"].values
    mu, sigma = summary.loc[["mean","std"],f"{delta_prefix}{column}"].values

    g = sns.jointplot(data=comparison_table, x=f"{column} {labelx}", y=f"{delta_prefix}{column}", kind="kde", marginal_kws=dict(alpha=1.0, color=margins_color),
                      levels=filled_levels, joint_kws=dict(fill=True, colors=filled_palette), height=7, xlim=xrange, ylim=yrange)

    g.ax_joint.axhline(ls=":", lw=1.0, color=guide_color)
    g.ax_joint.axhline(mu, ls="-", lw=1.0, color=guide_color)
    g.ax_joint.axhline(mu-sigma, ls="--", lw=1.0, color=guide_color)
    g.ax_joint.axhline(mu+sigma, ls="--", lw=1.0, color=guide_color)

    g.ax_marg_y.axhline(ls=":", lw=1.0, color=guide_color)
    g.ax_marg_y.axhline(mu, ls="-", lw=1.0, color=guide_color)
    g.ax_marg_y.axhline(mu-sigma, ls="--", lw=1.0, color=guide_color)
    g.ax_marg_y.axhline(mu+sigma, ls="--", lw=1.0, color=guide_color)

    column_name = column.replace(f"~({unit})", "") if unit else column
    g.ax_joint.text(0.05, 0.05, f"{delta_prefix}{column_name}$\,={mu:.2f}\pm{sigma:.2f}\,${unit}", ha="left", size="small", transform=g.ax_joint.transAxes)

    sns.kdeplot(data=comparison_table, x=f"{column} {labelx}", y=f"{delta_prefix}{column}", levels=dashed_levels, color=dashed_color, linestyles="--", linewidths=1.5, ax=g.ax_joint)

    ax_con = g.fig.add_axes([0.57, 0.57, 0.25, 0.25])
    divider = make_axes_locatable(ax_con)
    ax_histx = divider.append_axes("top", .2, pad=0.1, sharex=ax_con)
    ax_histy = divider.append_axes("right", .2, pad=0.1, sharey=ax_con)

    sns.despine(ax=ax_con)
    sns.despine(ax=ax_histx, left=True)
    sns.despine(ax=ax_histy, bottom=True)
    ax_con.tick_params(labelsize="xx-small", length=4)
    ax_histx.tick_params(labelsize="xx-small", length=4, labelbottom=False, labelleft=False, left=False)
    ax_histy.tick_params(labelsize="xx-small", length=4, labelleft=False, labelbottom=False, bottom=False)

    sns.kdeplot(x=comparison_table[f"{column} {labelx}"], alpha=1.0, color=margins_color, fill=True, ax=ax_histx)
    sns.kdeplot(y=comparison_table[f"{column} {labely}"], alpha=1.0, color=margins_color, fill=True, ax=ax_histy)
    sns.kdeplot(x=comparison_table[f"{column} {labelx}"], y=comparison_table[f"{column} {labely}"], levels=filled_levels, colors=filled_palette, fill=True, ax=ax_con)
    sns.kdeplot(data=comparison_table, x=f"{column} {labelx}", y=f"{column} {labely}", levels=dashed_levels, color=dashed_color, linestyles="--", linewidths=1.5, ax=ax_con)

    ax_con.plot(xrange, xrange, "-", lw=0.7, color=guide_color)
    if is_logscale:
        xscale = np.log10(np.linspace(*(10**xrange)))
        ax_con.plot(xscale, np.log10(10**xscale * 1.1), "--", lw=0.7, color=guide_color)
        ax_con.plot(xscale, np.log10(10**xscale / 1.1), "--", lw=0.7, color=guide_color)
    else:
        ax_con.plot(xrange, xrange+np.abs(0.1*xrange), "--", lw=0.7, color=guide_color)
        ax_con.plot(xrange, xrange-np.abs(0.1*xrange), "--", lw=0.7, color=guide_color)

    ax_histx.axvline(mu_a, ls="-", lw=0.7, color=guide_color)
    ax_histx.axvline(mu_a-sigma_a, ls="--", lw=0.7, color=guide_color)
    ax_histx.axvline(mu_a+sigma_a, ls="--", lw=0.7, color=guide_color)
    ax_histy.axhline(mu_b, ls="-", lw=0.7, color=guide_color)
    ax_histy.axhline(mu_b-sigma_b, ls="--", lw=0.7, color=guide_color)
    ax_histy.axhline(mu_b+sigma_b, ls="--", lw=0.7, color=guide_color)

    ax_con.set_xlabel(ax_con.get_xlabel(), size="x-small")
    ax_con.set_ylabel(ax_con.get_ylabel(), size="x-small")
    ax_con.set_xlim(xrange)
    ax_con.set_ylim(xrange)

    return g
