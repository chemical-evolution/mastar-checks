import numpy as np
from matplotlib import colors as mpl_colors
from matplotlib import rc
from cycler import cycler
import seaborn as sns


clist = "#114477 #117755 #E8601C #771111 #771144 #4477AA #44AA88 #F1932D #AA4477 #774411 #777711 #AA4455".split()
ccycle = cycler("color", clist)

latex_preamble = "\n".join([
    r"\usepackage{cmbright}",
    r"\usepackage[T1]{fontenc}"
])

font = {"family":"sans-serif", "sans-serif":"Open Sans", "size":20, "weight":300}
text = {"usetex":True, "latex.preamble":latex_preamble, "hinting":"native"}

rc("figure", figsize=(10, 10))
rc("text", **text)
# rc("font", **font)
rc("axes", linewidth=1.0, labelsize="small", titlesize="small", labelweight=300)#, prop_cycle=ccycle)
rc("xtick", labelsize="x-small")
rc("xtick.major", width=1.0)
rc("ytick", labelsize="x-small")
rc("ytick.major", width=1.0)
rc("lines", linewidth=2.0, markeredgewidth=0.0, markersize=7)
rc("patch", linewidth=0.0)
rc("legend", numpoints=1, scatterpoints=1, fontsize="x-small", title_fontsize="small", handletextpad=0.4, handlelength=1, handleheight=1, frameon=False)
rc("savefig", dpi=92, format="pdf")

MILES_COLOR = "#CA7576"
INDOUS_COLOR = "#6E9D89"

GSL_COLOR = "#2D92D6"
BOSZ_COLOR = "#3AC569"
Y19_COLOR = "#E74E77"

APOGEE_COLOR = "#57967F"
GAIA_COLOR = "#FE7664"

LIGHT_COLOR = "#E3DEDE"
MED_COLOR = "#8284A5"
MASTAR_COLOR = "#574E6C"
MASTAR_CMAP = sns.color_palette(f"blend:{LIGHT_COLOR},{MASTAR_COLOR}")
QUARTILE_PALETTE = sns.color_palette(f"blend:{LIGHT_COLOR},{MASTAR_COLOR}", n_colors=3)
# sns.diverging_palette(145, 275, l=30, n=15)
# sns.color_palette(f"blend:{LIGHT_COLOR},{COLOR}", n_colors=7)
