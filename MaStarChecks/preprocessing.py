
import numpy as np
import pandas as pd
from scipy.signal import convolve
from tqdm import tqdm
from sklearn.impute import KNNImputer
from sklearn.neighbors import LocalOutlierFactor
from sklearn.model_selection import train_test_split

from astroML.utils.decorators import pickle_results

from MaStarChecks.constants import BAR_FORMAT


def detect_outliers(seds, n_neighbors=20, mask=None):
    """Return a cleaned version of the given SEDs dataframe

    The cleaning consist in detecting and removing outliers, the resulting outliers
    will be masked as NaN values.

    Parameters
    ----------
    seds: pd.DataFrame
        A dataframe containing the stellar SEDs to be cleaned. Each row represents
        one SED, with the column names being the wavelengths and the index being the
        ID of the SEDs.
    n_neighbors: integer
        The number of neighbors to take when detecting the outliers.
    mask: array_like
        A boolean array to use as a mask for the values to impute. If None, all values <= 0
        will be imputed.

    Returns
    -------
    seds_clean: pd.DataFrame
        A cleaned version of the initial SEDs dataframe with outliers removed.
    """
    seds_masked = seds.mask(seds<=0.0 if mask is None else mask)
    seds_masked = seds_masked.mask(np.isinf(seds_masked))

    clf = LocalOutlierFactor(n_neighbors=n_neighbors)
    outliers = pd.DataFrame(index=seds_masked.index, columns=seds_masked.columns)
    iterator = tqdm(
        seds_masked.iterrows(),
        total=seds.shape[0],
        desc="detecting outliers",
        unit="SED",
        ascii=True,
        dynamic_ncols=True,
        bar_format=BAR_FORMAT
    )
    for index, sed in iterator:
        # remove NANs from SED
        sed_ = sed.dropna()
        # predict the outliers in the SED without NANs
        outliers.loc[index, :] = pd.Series(index=sed_.index, data=clf.fit_predict(sed_.values[:, None]))

    # after detecting outliers some fields will be empty (NANs)
    # fill them with the no-outlier mark
    outliers = outliers.fillna(-1)
    # build the outliers mask
    outliers_mask = outliers < 0

    return outliers_mask

def fill_missing(seds, weights="distance", mask=None):
    """Return a cleaned version of the given SEDs dataframe

    The cleaning consist in running an imputation algorithm to fill
    in the empty values.

    Parameters
    ----------
    seds: pd.DataFrame
        A dataframe containing the stellar SEDs to be cleaned. Each row represents
        one SED, with the column names being the wavelengths and the index being the
        ID of the SEDs.
    weights: string
        The type of weights to use when averaging the neighbors in the imputation
        process. It can take the value of "distance" (default) to use the distance
        between the neighbors and uniform to compute a normal average.
    mask: array_like
        A boolean array to use as a mask for the values to impute. If None, all
        values <= 0 will be imputed.

    Returns
    -------
    seds_clean: pd.DataFrame
        A cleaned version of the initial SEDs dataframe missing values imputed.
    """
    seds_masked = seds.mask(seds<=0.0 if mask is None else mask)
    seds_masked = seds_masked.mask(np.isinf(seds_masked))

    imp = KNNImputer(weights=weights)
    # fill empty values
    seds_clean = pd.DataFrame(
        index=seds_masked.index,
        columns=seds_masked.dropna(how="all", axis="columns").columns,
        data=imp.fit_transform(seds_masked)
    )
    return seds_clean

def clean_seds(seds, n_neighbors=20, weights="distance", mask=None):
    """Return a cleaned version of the given SEDs dataframe

    The cleaning consist in detecting and removing outliers and then
    running an imputation algorithm to fill in the empty values, those
    coming from the begining in the 'seds' and those produced during
    the outliers removal.

    Parameters
    ----------
    seds: pd.DataFrame
        A dataframe containing the stellar SEDs to be cleaned. Each row represents
        one SED, with the column names being the wavelengths and the index being the
        ID of the SEDs.
    n_neighbors: integer
        The number of neighbors to take when detecting the outliers.
    weights: string
        The type of weights to use when averaging the neighbors in the imputation
        process. It can take the value of "distance" (default) to use the distance
        between the neighbors and uniform to compute a normal average.
    mask: array_like
        A boolean array to use as a mask for the values to impute. If None, all values <= 0
        will be imputed.

    Returns
    -------
    seds_clean: pd.DataFrame
        A cleaned version of the initial SEDs dataframe with outliers removed and
        missing values imputed.
    """
    seds_masked = seds.mask(seds<=0.0 if mask is None else mask)

    clf = LocalOutlierFactor(n_neighbors=n_neighbors)
    imp = KNNImputer(weights=weights)

    outliers = pd.DataFrame(index=seds_masked.index, columns=seds_masked.columns)
    for index, sed in tqdm(seds_masked.iterrows(), total=seds.shape[0], desc="detecting outliers", unit="SED", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
        # remove NANs from SED
        sed_ = sed.dropna()
        # predict the outliers in the SED without NANs
        outliers.loc[index, :] = pd.Series(index=sed_.index, data=clf.fit_predict(sed_.values[:, None]))

    # after detecting outliers some fields will be empty (NANs)
    # fill them with the no-outlier mark
    outliers = outliers.fillna(-1)
    # build the outliers mask
    outliers_mask = outliers < 0
    # build the features mask where all features marked as outliers
    # in all rows will be removed since they cannot be imputed
    features_mask = ~outliers_mask.all(axis="index")

    # remove outliers and fill empty values
    seds_clean = pd.DataFrame(
        index=seds_masked.index,
        columns=seds_masked.columns[features_mask],
        data=imp.fit_transform(seds_masked.mask(outliers_mask))
    )
    return seds_clean

def resample_rss(new_wavelength, wavelength, rss):
    new_rss = np.zeros((rss.shape[0],new_wavelength.size))
    for i in range(len(new_rss)):
        new_rss[i] = np.interp(
            new_wavelength,
            wavelength,
            rss[i],
        )
    return new_rss

def gaussian_kernel(sigma, half_box=50):
    N = 2*half_box + 1
    kernel = np.exp(-0.5*(((np.arange(N) - half_box)/sigma)**2))
    return kernel / kernel.sum()

def downgrade_resolution(wavelength, spectrum, sigma, verbose=True):

    # assuming the sampling is uniform
    dwl = np.diff(wavelength)[0]

    if np.isscalar(sigma):
        sigma_ = sigma/dwl

        if 2.355*sigma_ < 2:
            return spectrum

        kernel = gaussian_kernel(sigma_)
        dwn_spectrum = convolve(spectrum, kernel, mode="same", method="fft")
    else:
        dwn_spectrum = []
        if verbose:
            iterator = tqdm(range(wavelength.size), desc="downgrading resolution", unit="pixel", ascii=True, bar_format=BAR_FORMAT)
        else:
            iterator = range(wavelength.size)
        for j in iterator:
            sigma_j = sigma[j]/dwl
            # if the sigma_j is smaller than the sampling rate, do not convolve
            if 2.355*sigma_j < 2:
                dwn_spectrum.append(spectrum[j])
                continue

            kernel = gaussian_kernel(sigma_j)
            conv_spectrum = convolve(spectrum, kernel, mode="same", method="fft")

            dwn_spectrum.append(conv_spectrum[j])
        dwn_spectrum = np.asarray(dwn_spectrum)

    return dwn_spectrum

def downgrade_resolution_rss(wavelength, rss, sigma, verbose=True):

    # assuming the sampling is uniform
    dwl = np.diff(wavelength)[0]

    if np.isscalar(sigma):
        sigma_ = sigma/dwl

        if 2.355*sigma_ < 2:
            return rss

        kernel_1d = gaussian_kernel(sigma_)
        kernel = np.zeros((3,kernel_1d.size))
        kernel[1] = kernel_1d

        dwn_rss = convolve(rss, kernel, mode="same", method="fft")
    else:
        dwn_rss = []
        if verbose:
            iterator = tqdm(range(wavelength.size), desc="downgrading resolution", unit="pixel", ascii=True, bar_format=BAR_FORMAT)
        else:
            iterator = range(wavelength.size)
        for j in iterator:
            sigma_j = sigma[j]/dwl
            # if the sigma_j is smaller than the sampling rate, do not convolve
            if 2.355*sigma_j < 2:
                dwn_rss.append(rss[:,j])
                continue

            kernel_1d = gaussian_kernel(sigma_j)
            kernel = np.zeros((3,kernel_1d.size))
            kernel[1] = kernel_1d
            conv_rss = convolve(rss, kernel, mode="same", method="fft")

            dwn_rss.append(conv_rss[:,j])
        dwn_rss = np.column_stack(dwn_rss)

    return dwn_rss

# @pickle_results("_cache/train-test.pkl")
def build_train_test(labels, test_ratio=0.10, random_state=123, verbose=True):
    labelled_set = labels.index[~labels.isna().any(axis="columns")]
    training_set, testing_set, _, _ = train_test_split(
        labelled_set, labelled_set,
        test_size=test_ratio, random_state=random_state
    )
    if verbose:
        print(f"labelled set : {labelled_set.size}")
        print(f"training set : {training_set.size}")
        print(f"testing set  : {testing_set.size}")

    return training_set, testing_set, labelled_set
