
import numpy as np
import scipy.stats as st


def gaussian_kde(support, mean, sdev, kernel_weights=None, h=1.0, return_kernels=False):

    dummy_k = np.full_like(support, np.nan, dtype=np.double)
    kernels = [dummy_k.copy() for i in range(mean.shape[0])]
    for k in range(mean.shape[0]):
        if np.isnan(mean[k]).any() or np.isnan(sdev[k]).any():
            continue
        else:
            kernel = (1/h) * st.multivariate_normal(mean=np.zeros(2), cov=np.diagflat(sdev[k])).pdf((support[0] - mean[k])/h)

        kernels[k] = kernel

    if kernel_weights is not None:
        pdf_ = np.nansum(kernel_weights[:, None] * np.asarray(kernels), axis=0)
    else:
        pdf_ = np.nansum(kernels, axis=0)

    if return_kernels:
        return pdf_, list(zip(kernel_weights, kernels))
    else:
        return pdf_
