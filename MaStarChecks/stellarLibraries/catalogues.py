
import warnings
import os, pickle
from tqdm import tqdm
import numpy as np
import pandas as pd

from astropy.table import Table, TableReplaceWarning
from astropy import units as u
from astropy.coordinates import SkyCoord
warnings.filterwarnings("ignore", category=TableReplaceWarning)

# from dustmaps.sfd import SFDQuery
# sfd = SFDQuery()
from dustmaps.bayestar import BayestarWebQuery
bst = BayestarWebQuery(version="bayestar2015")

from astropy.io import fits, ascii

from MaStarChecks.constants import CACHE_PATH, CARDS, BAR_FORMAT
from MaStarChecks.constants import INDOUS_CATALOGUE_PATH
from MaStarChecks.constants import MILES_REFERENCE_PATH, MILES_CATALOGUE_PATH, MILES_PARAMETER_PATH
from MaStarChecks.constants import XSHOOTER_REFERENCE_PATH, XSHOOTER_CATALOGUE_PATH, XSHOOTER_PARAMETER_PATH
from MaStarChecks.constants import MASTAR_CATALOGUE_PATH, MASTAR_GAIA_PATH
from MaStarChecks.constants import MWM_CATALOGUE_PATH
from MaStarChecks.constants import GSL_SEDS_PATH, BOSZ_SEDS_PATH


def build_indous_catalogue(use_cache=True, cache_path=CACHE_PATH):
    """Return the IndoUS catalogue including stellar parameters

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.

    Returns
    -------
        pd.DataFrame
            A dataframe containing the reference between the SED
            names and the catalogue name.
        pd.DataFrame
            A dataframe containing the catalogue information,
            including the RA and Dec and some photometric
            columns.
        pd.DataFrame
            A dataframe containing physical properties columns
            from the FITS file.
    """

    cache_file = os.path.join(cache_path, "indous-catalogue.csv")

    if use_cache and os.path.isfile(cache_file):
        indous_catalogue = pd.read_csv(cache_file)
    else:
        indous = fits.open(INDOUS_CATALOGUE_PATH)
        arr = indous[1].data
        indous_catalogue = pd.DataFrame()
        for column in arr.names:
            indous_catalogue[column] = arr[column]

        columns_mapping = {
            "TITLE": "ID",
            "OBJRA": "RA",
            "OBJDEC": "DEC",
            "FEH": "MET",
            "OBJBMAG": "BMAG",
            "OBJVMAG": "VMAG",
            "PICKTYPE": "SPTYPE",
            "PHYSREF": "BIBREF",
            "OBJRV": "RADVEL"
        }
        indous_catalogue = indous_catalogue.drop(columns=["COVERAGE", "GAPS", "NCOMBINE", "SPTYPE"])
        indous_catalogue = indous_catalogue.rename(columns=columns_mapping)
        indous_catalogue.ID = indous_catalogue.ID.apply(lambda s: s.strip())
        coords = SkyCoord(indous_catalogue.RA, indous_catalogue.DEC, unit=(u.hourangle, u.degree))
        indous_catalogue.RA = coords.ra.to_string(unit=u.hourangle, pad=True, precision=2)
        indous_catalogue.DEC = coords.dec.to_string(unit=u.deg, pad=True, precision=2, alwayssign=True)

        indous_catalogue.to_csv(cache_file, index=False)

    return indous_catalogue

def build_miles_catalogue(use_cache=True, cache_path=CACHE_PATH):
    """Return the MILES catalogue, reference and physical parameters

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.

    Returns
    -------
        pd.DataFrame
            A dataframe containing the reference between the SED
            names and the catalogue name.
        pd.DataFrame
            A dataframe containing the catalogue information,
            including the RA and Dec and some photometric
            columns.
        pd.DataFrame
            A dataframe containing physical properties columns
            from the FITS file.
    """

    cache_file = os.path.join(cache_path, "miles-catalogue.csv")

    if use_cache and os.path.isfile(cache_file):
        miles_catalogue = pd.read_csv(cache_file)
    else:
        par_columns = {
            "MILES": "ID",
            "[Fe/H]": "MET",
            "Teff": "TEFF",
            "logg": "LOGG",
            "Name": "BIBREF"
        }
        miles_parameter = ascii.read(
            table=MILES_PARAMETER_PATH,
            format="fixed_width",
            col_starts=(0, 9, 17, 24, 32, 49)
        ).to_pandas()
        miles_parameter.MILES = miles_parameter.MILES.str.strip()
        miles_parameter = miles_parameter.filter(items=par_columns.keys())
        miles_parameter = miles_parameter.rename(columns=par_columns)
        miles_parameter = miles_parameter.set_index("ID").sort_index()

        ref_columns = {
            "MILES": "ID",
            "SpT": "SPTYPE",
            "Name": "NAMES",
        }
        miles_reference = ascii.read(
            table=MILES_REFERENCE_PATH,
            format="fixed_width",
            col_starts=(0, 9, 17, 24, 32, 50, 67)
        ).to_pandas()
        miles_reference.MILES = miles_reference.MILES.str.strip()
        miles_reference = miles_reference.filter(items=ref_columns.keys())
        miles_reference = miles_reference.rename(columns=ref_columns)
        miles_reference = miles_reference.set_index("ID").sort_index()

        miles_catalogue = pd.concat((miles_reference,miles_parameter), axis="columns", sort=True)
        miles_catalogue = miles_catalogue.reset_index().rename(columns={"index": "ID"})

        miles_catalogue.to_csv(cache_file, index=False)

    return miles_catalogue

def build_xshooter_catalogue(use_cache=True, cache_path=CACHE_PATH):
    """Return the X-shooter catalogue including stellar parameters

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.

    Returns
    -------
        pd.DataFrame
            A dataframe containing the reference between the SED
            names and the catalogue name.
        pd.DataFrame
            A dataframe containing the catalogue information,
            including the RA and Dec and some photometric
            columns.
        pd.DataFrame
            A dataframe containing physical properties columns
            from the FITS file.
    """

    cache_file = os.path.join(cache_path, "xshooter-catalogue.p")

    if use_cache and os.path.isfile(cache_file):
        xshooter_reference, xshooter_catalogue, xshooter_parameter = pickle.load(open(cache_file, "rb"))
    else:
        # REFERENCE --------------------------------------------------------------------------------------
        xshooter_reference = ascii.read(
            table=XSHOOTER_REFERENCE_PATH,
            format="fixed_width_no_header",
            col_starts=(0, 26, 32, 37, 40, 42, 52, 63, 66, 68, 80, 91, 94),
            names="HNAME Tefflit e_Tefflit n_Tefflit q_Tefflit logglit e_logglit n_logglit q_logglit [Fe/H]lit e_[Fe/H]lit n_[Fe/H]lit q_[Fe/H]lit".split(),
        ).to_pandas().set_index("HNAME")

        xshooter_reference.index = map(str.strip, xshooter_reference.index)
        xshooter_reference = xshooter_reference.sort_index().rename_axis(index="ID")

        # CATALOGUE --------------------------------------------------------------------------------------
        xshooter_catalogue = ascii.read(
            table=XSHOOTER_CATALOGUE_PATH,
            format="fixed_width_no_header",
            col_starts=(0, 24, 25, 27, 29, 33, 34, 36, 38, 41, 47, 53, 59, 64, 70, 76, 78, 80, 82),
            names="HNAME J RAh RAm RAs DE- DEd DEm DEs Teff e_Teff logg e_logg [Fe/H] e_[Fe/H] r c m Ref".split(),
        ).to_pandas().set_index("HNAME")

        xshooter_catalogue.index = map(str.strip, xshooter_catalogue.index)
        xshooter_catalogue = xshooter_catalogue.sort_index().rename_axis(index="ID")

        de_data = xshooter_catalogue.filter(like="DE")
        ra_data = xshooter_catalogue.filter(like="RA")
        xshooter_catalogue = xshooter_catalogue.drop(columns=de_data.columns)
        xshooter_catalogue = xshooter_catalogue.drop(columns=ra_data.columns)

        xshooter_catalogue["dec"] = de_data.apply(lambda r: "{}{:02d}:{:02d}:{:02d}".format(*r.tolist()), axis="columns")
        xshooter_catalogue["ra"] = ra_data.apply(lambda r: "{:02d}:{:02d}:{:.1f}".format(*map(lambda x: int(x) if int(x) - x == 0 else x, r.tolist())), axis="columns")

        # PARAMETER --------------------------------------------------------------------------------------
        xshooter_parameter = ascii.read(
            table=XSHOOTER_PARAMETER_PATH,
            guess=False,
            format="fixed_width_no_header",
            col_starts=(0, 26, 30, 36, 42, 48, 54, 60, 66, 72, 77, 83, 88, 94, 99, 101, 103, 105),
            names="HNAME ID_ Teffuvb logguvb [Fe/H]uvb Teffvis loggvis [Fe/H]vis Teff e_Teff logg e_logg [Fe/H] e_[Fe/H] f_Teff f_logg f_[Fe/H] Cflag".split(),
        ).to_pandas().set_index("HNAME")

        xshooter_parameter.index = map(str.strip, xshooter_parameter.index)
        xshooter_parameter = xshooter_parameter.sort_index().rename_axis(index="ID")

        # MANIPULATION ------------------------------------------------------------------------------------
        xshooter_catalogue = xshooter_catalogue.drop(columns=["J"])

        pickle.dump((xshooter_reference, xshooter_catalogue, xshooter_parameter), open(cache_file, "wb"))

    return xshooter_reference, xshooter_catalogue, xshooter_parameter

def build_mastar_catalogue(use_cache=True, cache_path=CACHE_PATH):
    """Return the catalogue of the MaStar stellar library using
    the FITS file `mastarall-v1[-v2].fits`.

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.

    Returns
    -------
        pd.DataFrame
            A dataframe containing physical properties columns
            from the FITS file.
    """

    cache_file = os.path.join(cache_path, "mastar-catalogue.csv")

    if use_cache and os.path.isfile(cache_file):
        mastar_catalogue = pd.read_csv(cache_file)
    else:
        catalogue_fits = fits.open(MASTAR_CATALOGUE_PATH)

        columns_mapping = dict(zip(
            ["MANGAID", "RA", "DEC", "PHOTOCAT", "INPUT_LOGG", "INPUT_TEFF", "INPUT_FE_H", "INPUT_ALPHA_M"],
            ["ID", "RA", "DEC", "PHOTCAT", "LOGG", "TEFF", "MET", "ALPHAM"]
        ))

        mastar_catalogue = pd.DataFrame(index=range(catalogue_fits["GOODSTARS"].header["NAXIS2"]), columns=columns_mapping.values())
        for cardname, column in columns_mapping.items():
            mastar_catalogue.loc[:, column] = catalogue_fits["GOODSTARS"].data[cardname]

        mastar_psf = pd.DataFrame(
            index=range(catalogue_fits["GOODSTARS"].header["NAXIS2"]),
            columns=[f"{band}SDSS" for band in list("UGRIZ")],
            data=catalogue_fits["GOODSTARS"].data["PSFMAG"]
        )
        mastar_catalogue = pd.concat((mastar_catalogue, mastar_psf), axis="columns")
        for column in mastar_catalogue.columns[mastar_catalogue.dtypes == object]:
            mastar_catalogue[column] = mastar_catalogue[column].str.strip()

        mastar_catalogue = mastar_catalogue.apply(lambda s: s.mask(s <= -999) if s.dtype != object else s)
        coords = SkyCoord(mastar_catalogue.RA, mastar_catalogue.DEC, unit=(u.degree, u.degree))
        mastar_catalogue.RA = coords.ra.to_string(unit=u.hourangle, pad=True, precision=2)
        mastar_catalogue.DEC = coords.dec.to_string(unit=u.deg, pad=True, precision=2, alwayssign=True)

        # additional information
        match_fits = fits.open(MASTAR_GAIA_PATH)

        match_columns = ["MANGAID", "R_EST"]
        match_mastar_gaia = Table(match_fits[1].data)[match_columns].to_pandas()
        match_mastar_gaia = match_mastar_gaia.rename(columns=lambda s:s.lower())
        match_mastar_gaia = match_mastar_gaia.rename(columns={"mangaid":"ID"})

        match_mastar_gaia.ID = match_mastar_gaia.ID.str.strip()
        match_mastar_gaia = match_mastar_gaia.loc[match_mastar_gaia.ID.isin(mastar_catalogue.ID)].reset_index(drop=True)
        match_mastar_gaia.r_est = match_mastar_gaia.r_est.mask(match_mastar_gaia.r_est<=0)
        match_mastar_gaia["DIST"] = match_mastar_gaia.r_est / 1000

        mastar_catalogue = mastar_catalogue.merge(match_mastar_gaia.get(["ID","DIST"]), left_on="ID", right_on="ID")
        mastar_catalogue["EBV"] = bst(SkyCoord(
            ra=mastar_catalogue.RA,
            dec=mastar_catalogue.DEC,
            distance=mastar_catalogue.DIST*u.kpc,
        ), mode="median")

        mastar_catalogue.to_csv(cache_file, index=False)

    return mastar_catalogue

def build_mwm_catalogue(use_cache=True, cache_path=CACHE_PATH):
    """Return the catalogue of the MWM stellar library using
    the FITS file `spAll-[version].fits`.

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.

    Returns
    -------
        pd.DataFrame
            A dataframe containing physical properties columns
            from the FITS file.
    """
    cache_file = os.path.join(cache_path, "mwm-catalogue.csv")

    if use_cache and os.path.isfile(cache_file):
        mwm_catalogue = pd.read_csv(cache_file)
    else:
        catalogue_fits = fits.open(MWM_CATALOGUE_PATH)

        columns_mapping = dict(zip(
            ["PLUG_RA", "PLUG_DEC", "CLASS", "SUBCLASS", "XCSAO_LOGG",
                "XCSAO_TEFF", "XCSAO_FEH", "GAIA_BP",
                "GAIA_RP", "GAIA_G", "GAIA_PARALLAX", "SN_MEDIAN_ALL", "Z", "Z_ERR", "FIRSTCARTON"],
            ["RA", "DEC", "CLASS", "SPTYPE", "LOGG", "TEFF",
                "MET", "GAIA_BP", "GAIA_RP", "GAIA_G", "DIST", "SNR", "RADVEL", "RVERR", "PHOTCAT"]
        ))
        catalogue_table = Table(catalogue_fits[1].data)[list(columns_mapping.keys())].to_pandas()

        mwm_catalogue = pd.DataFrame(index=range(len(catalogue_table)), columns=columns_mapping.values())
        for cardname, column in columns_mapping.items():
            mwm_catalogue[column] = catalogue_table[cardname]
        # fix given columns
        mwm_catalogue.RADVEL = 3e5 * mwm_catalogue["RADVEL"]
        mwm_catalogue.RVERR = 3e5 * mwm_catalogue["RVERR"]
        mwm_catalogue.DIST = 1000 / mwm_catalogue.DIST
        mwm_catalogue.DIST = mwm_catalogue.DIST.mask(mwm_catalogue.DIST <= 0)
        # add missing columns
        mwm_catalogue["ID"] = [f"{row['PLATE']:05d}-{row['MJD']}-{row['CATALOGID']:011d}" for row in catalogue_fits[1].data]
        mwm_catalogue["USDSS"] = catalogue_fits[1].data["MAG"][:, 0]
        mwm_catalogue["GSDSS"] = catalogue_fits[1].data["MAG"][:, 1]
        mwm_catalogue["RSDSS"] = catalogue_fits[1].data["MAG"][:, 2]
        mwm_catalogue["ISDSS"] = catalogue_fits[1].data["MAG"][:, 3]
        mwm_catalogue["ZSDSS"] = catalogue_fits[1].data["MAG"][:, 4]

        mwm_catalogue["EBV"] = bst(SkyCoord(
            ra=mwm_catalogue.RA*u.deg,
            dec=mwm_catalogue.DEC*u.deg,
            distance=mwm_catalogue.DIST*u.kpc,
        ), mode="median")
        
        mwm_catalogue = mwm_catalogue.loc[mwm_catalogue.CLASS.str.strip() == "STAR"].reset_index()
        mwm_catalogue.to_csv(cache_file, index=False)

    return mwm_catalogue

def build_gsl_catalogue(use_cache=True, cache_path=CACHE_PATH):
    """Return the GSL catalogue including stellar parameters

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.

    Returns
    -------
        pd.DataFrame
            A dataframe containing the reference between the SED
            names and the catalogue name.
        pd.DataFrame
            A dataframe containing the catalogue information,
            including the RA and Dec and some photometric
            columns.
        pd.DataFrame
            A dataframe containing physical properties columns
            from the FITS file.
    """

    cache_file = os.path.join(cache_path, "gsl-catalogue.csv")

    if use_cache and os.path.isfile(cache_file):
        gsl_catalogue = pd.read_csv(cache_file)
    else:
        fits_list = sorted([os.path.join(root,fitsname) for root, subs, files in os.walk(GSL_SEDS_PATH) for fitsname in files if fitsname.endswith(".fits")])
        table = []
        for fitsname in tqdm(fits_list, desc="building GSL catalogue", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
            hdr = fits.getheader(fitsname)
            id_ = os.path.basename(fitsname).replace(".PHOENIX-ACES-AGSS-COND-2011-HiRes.fits", "")

            row = {"ID":id_}
            row["TEFF"] = hdr.get("PHXTEFF")
            row["LOGG"] = hdr.get("PHXLOGG")
            row["MET"] = hdr.get("PHXM_H")
            row["ALPHAM"] = hdr.get("PHXALPHA")
            row["PHXXI_L"] = hdr.get("PHXXI_L")
            row["PHXXI_M"] = hdr.get("PHXXI_M")
            row["PHXXI_N"] = hdr.get("PHXXI_N")
            row["PHXMASS"] = hdr.get("PHXMASS")
            row["PHXREFF"] = hdr.get("PHXREFF")
            row["PHXLUM"] = hdr.get("PHXLUM")
            row["PHXMXLEN"] = hdr.get("PHXMXLEN")
            table.append(row)

        gsl_catalogue = pd.DataFrame(table)
        gsl_catalogue.to_csv(cache_file, index=False)

    return gsl_catalogue

def build_bosz_catalogue(use_cache=True, cache_path=CACHE_PATH):
    cache_file = os.path.join(cache_path, "bosz-catalogue.csv")

    if use_cache and os.path.isfile(cache_file):
        bosz_catalogue = pd.read_csv(cache_file)
    else:
        fits_list = sorted([os.path.join(root,fitsname) for root, subs, files in os.walk(BOSZ_SEDS_PATH) for fitsname in files if fitsname.endswith(".fits")])
        table = []
        for fitsname in tqdm(fits_list, desc="building BOSZ catalogue", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
            hdr = fits.getheader(fitsname)
            id_ = os.path.basename(fitsname).replace(".fits", "")

            row = {"ID":id_}
            row["TEFF"] = hdr["T_EFF"]
            row["LOGG"] = hdr["LOGG"]
            row["MET"] = hdr["MH"]
            row["ALPHAM"] = hdr["ALPHA"]
            row["CM"] = hdr["CM"]
            table.append(row)

        bosz_catalogue = pd.DataFrame(table)
        bosz_catalogue.to_csv(cache_file, index=False)

    return bosz_catalogue

def extract_catalogue(hdus, library_name):
    catalogue = []
    for hdu in tqdm(hdus, desc=f"extracting {library_name} catalogue", unit="record", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
        catalogue.append(pd.Series(*tuple(zip(*filter(lambda item: item[0] in tuple(zip(*CARDS))[0], hdu[0].header.items())))[::-1]))
    return pd.DataFrame(catalogue)
