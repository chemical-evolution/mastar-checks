
import warnings
import os, pickle
import numpy as np
import pandas as pd
from copy import copy
from tqdm import tqdm
from astropy.io import fits
from astropy.table import Table
from astropy.utils.exceptions import AstropyDeprecationWarning

warnings.filterwarnings("ignore", category=AstropyDeprecationWarning)

from astropy import units as u

from dust_extinction.parameter_averages import F99

RV = 3.1
ext = F99(Rv=RV)

from MaStarChecks.constants import CACHE_PATH, HOMOGENEOUS_DATA_PATH, MWM_SEDS_PATH
from MaStarChecks.constants import SPECTRA_TYPES, CARDS, BAR_FORMAT, NAN_PLACEHOLDER
from MaStarChecks.constants import MILES_PATH
from MaStarChecks.constants import INDOUS_SEDS_PATH
from MaStarChecks.constants import MASTAR_SEDS_PATH, MASTAR_GAIA_PATH
from MaStarChecks.constants import XSHOOTER_SEDS_PATH
from MaStarChecks.constants import GSL_FWHM, GSL_WAVE_PATH, GSL_SEDS_PATH
from MaStarChecks.constants import BOSZ_SEDS_PATH
from MaStarChecks.constants import BITS2FLAG
from MaStarChecks.constants import GZP, BPZP, RPZP, GAIA_GBAND_PATH, GAIA_BPBAND_PATH, GAIA_RPBAND_PATH

from MaStarChecks.common import has_any_bits
from MaStarChecks.stellarLibraries.photometry import mag
from MaStarChecks.preprocessing import downgrade_resolution


def build_indous_library(catalogue, output_path=HOMOGENEOUS_DATA_PATH, use_cache=True):
    hdus = []
    for idx, params in tqdm(catalogue.iterrows(), total=catalogue.index.size, desc="building IndoUS library", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
        orig_name = os.path.join(INDOUS_SEDS_PATH, f"{params.ID}.fits.gz")
        dest_name = os.path.join(output_path, f"indous-{params.ID}.fits.gz")
        if use_cache and os.path.isfile(dest_name):
            hdus.append(fits.open(dest_name, memmap=False)[1])
        else:
            hdr = fits.Header()
            for (name, comment) in CARDS:
                value = params.get(name, NAN_PLACEHOLDER)
                hdr.append((name, value.strip() if isinstance(value,str) else value, comment))

            if os.path.isfile(orig_name):
                with fits.open(orig_name, memmap=False) as f:
                    wl, fl = f[1].data["wavelength"].squeeze(), f[1].data["spectrum"].squeeze()
                    fl[fl==0.0001] = 0.0
            else:
                tqdm.write(f"Unable to locate spectrum '{params.ID}'")
                continue

            wl_col = fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl)
            fl_col = fits.Column(name=SPECTRA_TYPES[0], unit="erg/s/cm^2/AA", format="E", array=fl)

            primary_hdu = fits.PrimaryHDU(header=hdr)
            table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs((wl_col,fl_col)), name="SPECTRUM")
            hdulist = fits.HDUList([primary_hdu, table_hdu])
            hdulist.writeto(dest_name, overwrite=True)

            hdus.append(hdulist)

    return hdus

def build_miles_library(catalogue, output_path=HOMOGENEOUS_DATA_PATH, use_cache=True, ext_corrected=True):
    """Return a dictionary containing the MILES SEDs, either corrected
    by the MW dust extinction (original MILES) or uncorrected.

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.
    ext_corrected: boolean
        Whether the SEDs will be corrected by dust extinction or
        not. Defaults to True.

    Returns
    -------
        dict
            A dictionary with keys as the IDs of the stars in MILES
            and the values a tuple the wavelength and flux.
    """
    miles_seds_path = os.path.join(MILES_PATH, "miles_seds" if ext_corrected else "miles_seds_uncorrected")
    hdus = []
    for idx, params in tqdm(catalogue.iterrows(), total=catalogue.index.size, desc="building MILES library", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
        orig_name = os.path.join(miles_seds_path, f"{params.ID}.fits.gz")
        dest_name = os.path.join(output_path, f"miles-{params.ID}.fits.gz")
        if use_cache and os.path.isfile(dest_name):
            hdus.append(fits.open(dest_name, memmap=False)[1])
        else:
            hdr = fits.Header()
            for (name, comment) in CARDS:
                value = params.get(name, NAN_PLACEHOLDER)
                hdr.append((name, value.strip() if isinstance(value,str) else value, comment))

            if os.path.isfile(orig_name):
                with fits.open(os.path.join(miles_seds_path, orig_name), memmap=False) as f:
                    wl = np.array([(i + 1) * f[0].header["CDELT1"] + f[0].header["CRVAL1"] for i in range(f[0].header["NAXIS1"])])
                    fl = f[0].data[0]
            else:
                tqdm.write(f"Unable to locate spectrum '{params.ID}'")
                continue

            wl_col = fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl)
            fl_col = fits.Column(name=SPECTRA_TYPES[0], unit="erg/s/cm^2/AA", format="E", array=fl)

            primary_hdu = fits.PrimaryHDU(header=hdr)
            table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs((wl_col,fl_col)), name="SPECTRUM")
            hdulist = fits.HDUList([primary_hdu, table_hdu])
            hdulist.writeto(dest_name, overwrite=True)

            hdus.append(hdulist)

    return hdus

def load_xshooter_seds(use_cache=True):
    """Return a dictionary containing the X-shooter SEDs.

    Parameters
    ----------
    use_cache: boolean
        Whether to use (True) or build the cache (False). Defaults
        to True.
    ext_corrected: boolean
        Whether the SEDs will be corrected by dust extinction or
        not. Defaults to True.

    Returns
    -------
        dict
            A dictionary with keys as the IDs of the stars in IndoUS
            and the values a tuple the wavelength and flux.
    """

    xshooter_seds_list = sorted(filter(lambda s: s.endswith(".fits.gz"), os.listdir(XSHOOTER_SEDS_PATH)))
    cache_seds = os.path.join(CACHE_PATH, "xshooter-seds.p")

    if use_cache and os.path.isfile(cache_seds):
        xshooter_seds = pickle.load(open(cache_seds, "rb"))
    else:
        xshooter_seds = {}
        for fitsfile in tqdm(xshooter_seds_list, desc="building X-Shooter library", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
            with fits.open(os.path.join(XSHOOTER_SEDS_PATH, fitsfile), memmap=False) as f:
                xshooter_seds[fitsfile.replace(".fits.gz", "")] = {
                    "wl": np.array([np.exp(f[0].header["CRVAL1"] + i * f[0].header["CDELT1"]) for i in range(f[0].header["NAXIS1"])]),
                    "fl": f[0].data,
                    "fe": f[1].data
                }

        pickle.dump(xshooter_seds, open(cache_seds, "wb"))

    return xshooter_seds

def build_mastar_library(catalogue, output_path=HOMOGENEOUS_DATA_PATH, use_cache=True, vac_to_air=True):

    # read Gaia matched catalogue & photometric passbands
    match_fits = fits.open(MASTAR_GAIA_PATH)
    match_columns = ["MANGAID", "SOURCE_ID", "RA", "DEC", "R_EST", "PHOT_G_MEAN_MAG", "BP_RP"]
    match_mastar_gaia = Table(match_fits[1].data)[match_columns].to_pandas()
    match_mastar_gaia = match_mastar_gaia.rename(columns=lambda s:s.lower())
    match_mastar_gaia = match_mastar_gaia.rename(columns={"mangaid":"ID"})
    match_mastar_gaia.ID = match_mastar_gaia.ID.str.strip()
    match_mastar_gaia.set_index("ID", inplace=True)

    gband = np.loadtxt(GAIA_GBAND_PATH)
    bpband = np.loadtxt(GAIA_BPBAND_PATH)
    rpband = np.loadtxt(GAIA_RPBAND_PATH)

    # read quality flags from SEDs FITS --------------------------------------------------------------------------
    seds_fits = fits.open(MASTAR_SEDS_PATH, memmap=True)

    quality_columns = dict(zip(
        ["MANGAID", "IFUDESIGN", "MJD", "NEXP_VISIT", "HELIOV", "VERR", "MJDQUAL", "MNGTARG2"],
        ["ID", "IFUDESIGN", "MJD", "NEXP", "RADVEL", "RVERR", "SPQUAL", "PHQUAL"]
    ))
    quality = pd.DataFrame(index=range(seds_fits[1].header["NAXIS2"]), columns=quality_columns.values())
    for card, column in quality_columns.items():
        quality.loc[:, column] = seds_fits[1].data[card]

    quality.ID = quality.ID.str.strip()
    quality.NEXP = quality.NEXP.astype(np.int32)
    quality = quality.reset_index().rename(columns={"index": "RECORD"})

    quality["SCI"] = ~seds_fits[1].data["IFUDESIGN"].startswith("7")
    quality["GOODSP"] = ~np.asarray(list(map(lambda number: has_any_bits(number, bits=BITS2FLAG), seds_fits[1].data["MJDQUAL"])))
    quality["GOODRV"] = quality.RADVEL != 0

    # ------------------------------------------------------------------------------------------------------------

    hdus = []
    iterator = tqdm(
        catalogue.iterrows(),
        total=catalogue.index.size,
        desc="building MaStar library", unit="star",
        ascii=True, dynamic_ncols=True,
        bar_format=BAR_FORMAT
    )
    for idx, params in iterator:
        for idy, qual_params in quality.loc[quality.ID==params.ID].iterrows():
            dest_name = os.path.join(output_path, f"mastar-{params.ID}-{qual_params.RECORD}.fits.gz")
            if use_cache and os.path.isfile(dest_name):
                hdus.append(fits.open(dest_name, memmap=False)[1])
            else:
                hdr = fits.Header()
                for (name, comment) in CARDS:
                    value = params.get(name, qual_params.get(name, NAN_PLACEHOLDER))
                    hdr.append((name, value.strip() if isinstance(value,str) else value, comment))

                wl = seds_fits[1].data["WAVE"][qual_params.RECORD]
                if vac_to_air:
                    sigma_2 = (1e4/wl)**2
                    f = 1.0 + 0.05792105 / (238.0185 - sigma_2) + 0.00167917 / (57.362 - sigma_2)
                    wl = wl / f

                fl = seds_fits[1].data["FLUX"][qual_params.RECORD]
                if params.EBV != NAN_PLACEHOLDER:
                    reddening = ext.extinguish(wl.astype(float)*u.AA, Ebv=params.EBV)
                else:
                    reddening = np.ones_like(wl)
                fl = np.divide(fl, reddening, where=reddening!=0, out=np.full_like(fl,fill_value=np.nan,dtype=np.double)) * 1e-17
                fe = seds_fits[1].data["IVAR"][qual_params.RECORD]
                fe = np.sqrt(np.divide(1, fe, where=fe>0.0, out=np.zeros_like(fe))) * 1e-17

                wl_col = fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl)
                fl_col = fits.Column(name=SPECTRA_TYPES[0], unit="erg/s/cm^2/AA", format="E", array=fl)
                fe_col = fits.Column(name=SPECTRA_TYPES[1], unit="erg/s/cm^2/AA", format="E", array=fe)
                mk_col = fits.Column(name=SPECTRA_TYPES[2], unit="", format="L", array=~seds_fits[1].data["MASK"][qual_params.RECORD].astype(bool))
                sg_col = fits.Column(name=SPECTRA_TYPES[3], unit="AA", format="E", array=seds_fits[1].data["DISP"][qual_params.RECORD])

                gmag = mag(np.column_stack((wl,fl)), gband, GZP)
                bpmag = mag(np.column_stack((wl,fl)), bpband, BPZP)
                rpmag = mag(np.column_stack((wl,fl)), rpband, RPZP)
                snr = np.nanmedian(np.divide(fl, fe, where=fe!=0, out=np.full_like(fl, np.nan, dtype=np.double)))

                hdr["GOODPH"] = (params.EBV!=NAN_PLACEHOLDER) & ((snr>=20) & (bpmag-rpmag>=-0.76) &
                    (not np.isnan([gmag,bpmag,rpmag,snr]).any()) &
                    (match_mastar_gaia.r_est.loc[params.ID]>0.0) &
                    (params.ID in match_mastar_gaia.mask(match_mastar_gaia==-999).dropna().index))

                primary_hdu = fits.PrimaryHDU(header=hdr)
                table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs((wl_col,fl_col,fe_col,mk_col,sg_col)), name="SPECTRA")
                hdulist = fits.HDUList([primary_hdu, table_hdu])
                hdulist.writeto(dest_name, overwrite=True)

                hdus.append(hdulist)

    return hdus

def build_mwm_library(catalogue, output_path=HOMOGENEOUS_DATA_PATH, use_cache=True, vac_to_air=True):

    hdus = []
    iterator = tqdm(
        catalogue.iterrows(),
        total=len(catalogue),
        desc="building MWM library", unit="star",
        ascii=True, dynamic_ncols=True,
        bar_format=BAR_FORMAT
    )
    for idx, params in iterator:
        dest_name = os.path.join(output_path, f"mwm-{params.ID}.fits.gz")
        if use_cache and os.path.isfile(dest_name):
            hdus.append(fits.open(dest_name, memmap=False)[1])
        else:
            hdr = fits.Header()
            for (name, comment) in CARDS:
                value = params.get(name, params.get(name, NAN_PLACEHOLDER))
                hdr.append((name, value.strip() if isinstance(value,str) else value, comment))
        name = f"spec-{params.ID}.fits.gz"
        sed_path = os.path.join(MWM_SEDS_PATH, name)
        try:
            sed_fits = fits.open(sed_path)
        except FileNotFoundError as e:
            # tqdm.write(f"spectrum {params.ID} not in the stellar library.")
            continue

        wl = 10**sed_fits[1].data["LOGLAM"]
        if vac_to_air and sed_fits[0].header["VACUUM"]:
            sigma_2 = (1e4/wl)**2
            f = 1.0 + 0.05792105 / (238.0185 - sigma_2) + 0.00167917 / (57.362 - sigma_2)
            wl = wl / f

        fl = sed_fits[1].data["FLUX"]
        if params.EBV != NAN_PLACEHOLDER:
            reddening = ext.extinguish(
                wl.astype(float)*u.AA, Ebv=params.EBV)
        else:
            reddening = np.ones_like(wl)
        fl = np.divide(fl, reddening, where=reddening!=0, out=np.full_like(fl,fill_value=np.nan,dtype=np.double)) * 1e-17
        fe = sed_fits[1].data["IVAR"]
        fe = np.sqrt(np.divide(1, fe, where=fe>0.0, out=np.zeros_like(fe))) * 1e-17

        wl_col = fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl)
        fl_col = fits.Column(name=SPECTRA_TYPES[0], unit="erg/s/cm^2/AA", format="E", array=fl)
        fe_col = fits.Column(name=SPECTRA_TYPES[1], unit="erg/s/cm^2/AA", format="E", array=fe)
        mk_col = fits.Column(name=SPECTRA_TYPES[2], unit="", format="L", array=~sed_fits[1].data["OR_MASK"].astype(bool))
        sg_col = fits.Column(name=SPECTRA_TYPES[3], unit="AA", format="E", array=sed_fits[1].data["WRESL"])

        primary_hdu = fits.PrimaryHDU(header=hdr)
        table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs((wl_col,fl_col,fe_col,mk_col,sg_col)), name="SPECTRA")
        hdulist = fits.HDUList([primary_hdu, table_hdu])
        hdulist.writeto(dest_name, overwrite=True)

        hdus.append(hdulist)
    
    return hdus

def build_gsl_library(catalogue, output_path=HOMOGENEOUS_DATA_PATH, use_cache=True, resample=True, vac_to_air=False):

    hdus = []
    for idx, params in tqdm(catalogue.iterrows(), total=catalogue.index.size, desc="building GSL library", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
        orig_name = os.path.join(os.path.join(GSL_SEDS_PATH, f"PHOENIX-ACES-AGSS-COND-2011_A1FITS_Z{params.ID[13:]}"), f"{params.ID}.PHOENIX-ACES-AGSS-COND-2011-HiRes.fits")
        dest_name = os.path.join(output_path, f"gsl-{params.ID}.fits.gz")
        if use_cache and os.path.isfile(dest_name):
            with fits.open(dest_name, memmap=False) as f:
                hdus.append(f[1])
        else:
            hdr = fits.Header()
            for (name, comment) in CARDS:
                value = params.get(name, NAN_PLACEHOLDER)
                hdr.append((name, value.strip() if isinstance(value,str) else value, comment))

            if os.path.isfile(orig_name):
                h = fits.getheader(orig_name)
                fl = fits.getdata(orig_name)

                # read wavelength grid either in log or linear space
                if h["CTYPE1"] == "AWAV-LOG":
                    wl = np.logspace(h["CRVAL1"], h["CRVAL1"]+h["NAXIS1"]*h["CDELT1"], h["NAXIS1"], base=np.exp(1))
                else:
                    wl = np.linspace(h["CRVAL1"], h["CRVAL1"]+h["NAXIS1"]*h["CDELT1"], h["NAXIS1"])

                # calculate air wavelengths
                # defaults to assume air wavelengths since they are called (CTYPE1) AWAV*
                if vac_to_air:
                    sigma_2 = (1e4/wl)**2
                    f = 1.0 + 0.05792105 / (238.0185 - sigma_2) + 0.00167917 / (57.362 - sigma_2)
                    wl = wl / f

                if resample:
                    wl_new = np.arange(3000, 10000+1, 1.0)
                    fl_new = np.interp(wl_new, wl, fl)
                    wl, fl = wl_new, fl_new
            else:
                tqdm.write(f"Unable to locate spectrum '{params.ID}'")
                continue

            wl_col = fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl)
            fl_col = fits.Column(name=SPECTRA_TYPES[0], unit="erg/s/cm^2/AA", format="E", array=fl)

            primary_hdu = fits.PrimaryHDU(header=hdr)
            table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs((wl_col,fl_col)), name="SPECTRUM")
            hdulist = fits.HDUList([primary_hdu, table_hdu])
            hdulist.writeto(dest_name, overwrite=True)

            hdus.append(hdulist)

    return hdus

def build_bosz_library(catalogue, output_path=HOMOGENEOUS_DATA_PATH, use_cache=True, resample=True, vac_to_air=False):
    hdus = []
    for idx, params in tqdm(catalogue.iterrows(), total=catalogue.index.size, desc="building BOSZ library", unit="star", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
        orig_name = os.path.join(BOSZ_SEDS_PATH, f"{params.ID}.fits")
        dest_name = os.path.join(output_path, f"bosz-{params.ID}.fits.gz")
        if use_cache and os.path.isfile(dest_name):
            hdus.append(fits.open(dest_name, memmap=False)[1])
        else:
            hdr = fits.Header()
            for (name, comment) in CARDS:
                value = params.get(name, NAN_PLACEHOLDER)
                hdr.append((name, value.strip() if isinstance(value,str) else value, comment))

            if os.path.isfile(orig_name):
                data = fits.getdata(orig_name, hdu=1)

                # read wavelength & flux
                wl = data["Wavelength"]
                fl = np.pi * data["SpecificIntensity"]

                # calculate air wavelengths
                # defaults to assume air wavelengths since they are called (CTYPE1) AWAV*
                if vac_to_air:
                    sigma_2 = (1e4/wl)**2
                    f = 1.0 + 0.05792105 / (238.0185 - sigma_2) + 0.00167917 / (57.362 - sigma_2)
                    wl = wl / f

                if resample:
                    wl_new = np.arange(3000, 10000+1, 1.0)
                    fl_new = np.interp(wl_new, wl, fl)
                    wl, fl = wl_new, fl_new
            else:
                tqdm.write(f"Unable to locate spectrum '{params.ID}'")
                continue

            wl_col = fits.Column(name="WAVELENGTH", unit="AA", format="E", array=wl)
            fl_col = fits.Column(name=SPECTRA_TYPES[0], unit="erg/s/cm^2/AA", format="E", array=fl)

            primary_hdu = fits.PrimaryHDU(header=hdr)
            table_hdu = fits.BinTableHDU.from_columns(fits.ColDefs((wl_col,fl_col)), name="SPECTRUM")
            hdulist = fits.HDUList([primary_hdu, table_hdu])
            hdulist.writeto(dest_name, overwrite=True)

            hdus.append(hdulist)

    return hdus

def extract_spectra(hdus, library_name, column="FLUX"):
    if column in hdus[0][1].data.columns.names:
        seds = []
        for hdu in tqdm(hdus, desc=f"extracting {column} from {library_name}", unit="SED", ascii=True, dynamic_ncols=True, bar_format=BAR_FORMAT):
            array = hdu[1].data
            seds.append(pd.Series(
                index=array["WAVELENGTH"].squeeze().astype(np.float),
                data=array[column].squeeze().astype(np.float)
            ))
        return pd.DataFrame(seds)
    return None
