<!--?xml version="1.0" encoding="UTF-8"?-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en"><head>
<title>MaStar Stellar Parameters</title>
<meta content="text/html; charset=UTF-8" http-equiv="Content-Type">

<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Quattrocento&family=Roboto:wght@100&display=swap" rel="stylesheet">

<link rel="stylesheet" href="index.css" >

<script src="https://polyfill.io/v3/polyfill.min.js?features=es6"></script>
<script id="MathJax-script" async src="https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"></script>
</head>
<body>
  <div class="flex-container">
    <header>
      <h1 id="overview">The MaNGA Stellar Library Physical Parameters</h1>
      <a href="#">Mejía-Narváez et al., 2021</a>
    </header>
    <nav>
      <ul>
       <li><a href="#summary">Summary</a></li>
       <li><a href="#data-format">Data Format</a></li>
       <li><a href="data/mastar-catalogue.fits.gz">Catalogue</a></li>
       <li><a href="#">CoSHA</a></li>
      </ul>
    </nav>
  </div>
  <article class="article">
    <div class="body">
      <h2 id="summary">Summary</h2>
      <p>
        In this site you will find the MaNGA Stellar Library (MaStar,
        <a href="https://arxiv.org/abs/1812.02745">Yan et al., 2019</a>) physical parameters
        computed using CoSHA, a Code for Stellar Heuristic Assigment of physical parameters:
        effective temperature (\(T_\mathrm{eff}\)), surface gravity (\(\log{g}\)), iron abundance
        (\([\mathrm{Fe}/\mathrm{H}]\)) and alpha to iron abundance (\([\alpha/\mathrm{Fe}]\)).
      </p>
      <p>
        CoSHA implements a Gradient Tree Boosting (GTB) algorithm to train a model for each property.
        GTB is a type of ensemble algorithm that trains a predefined number of decision trees in a
        serial way, so that each decision tree improves its predecessor. We used the GTB
        implementation in <a href="https://scikit-learn.org/">scikit-learn</a>. Since we took an
        heuristic approach we did not changed the hyperparameters
        <a href="https://scikit-learn.org/stable/modules/generated/sklearn.ensemble.GradientBoostingRegressor.html#sklearn.ensemble.GradientBoostingRegressor">default values</a>.
        In order to test de realiability of CoSHA we compared our results using external data sets
        such as APOGEE and tried to reproduce the known spatial trends in well-studied planes such
        as \([\alpha/\mathrm{Fe}]\) versus \([\mathrm{Fe}/\mathrm{H}]\).
      </p>
      <p>
        To train/test CoSHA we used the first data release of MaStar
        (<a href="https://arxiv.org/abs/1812.02745">Yan et al., 2019</a>) and the Göttingen Spectral
        Library (GSL, <a href="https://arxiv.org/abs/1303.5632">Husser et al., 2013</a>). Both data
        sets comprise ~30k stars allowing for a wide parameter space coverage:
      </p>
      <table border="1" class="dataframe">
        <thead>
          <tr style="text-align: right;">
            <th></th>
            <th>min</th>
            <th>max</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>\(T_\mathrm{eff}\) (K)</th>
            <td>2300</td>
            <td>12000</td>
          </tr>
          <tr>
            <th>\(\log{g}\)</th>
            <td>-0.025</td>
            <td>6.000</td>
          </tr>
          <tr>
            <th>\([\mathrm{Fe}/\mathrm{H}]\)</th>
            <td>-4.0</td>
            <td>1.0</td>
          </tr>
          <tr>
            <th>\([\alpha/\mathrm{Fe}]\)</th>
            <td>-0.2</td>
            <td>1.2</td>
          </tr>
        </tbody>
      </table>
      <p>
        There are some limitations to using such data set to train CoSHA, being the most important
        the \(T_\mathrm{eff}\) coverage. We expect to expand the parameter space coverage in future
        updates of the catalogue. See <a href="#">Mejía-Narváez et al., 2021</a> for a discussion on
        other potential limitations.
      </p>
      <p>
        Despite the above-mentioned limitations in the parameter space coverage, CoSHA recovers with
        high confidence most important parameter space distribution features.
        <div class="polaroid">
          <img class="cardimg" src="images/parameter-distributions.png" alt="parameter space distribution">
          <div class="caption">
            <span class="caption-label">Figure 1.</span> The parameter space distribution of stellar
            properties for MaStar as predicted by CoSHA. The binned distribution represents the
            actual sampling of the parameter space by MaStar, while the contours represent the volume
            corrected distribution. The contours enclose the 25, 50 and 75% (solid line) and the 95%
            (dashed line) distribution density. We show the average spectra in both cases as well.
          </div>
        </div>
      </p>

      <hr>

      <h2 id="data-format">Data Format</h2>
      <p>
        The catalogue comprises the atmospheric properties of over 22k stars from MaStar, as well as
        other physical and observed properties in a FITS file binary table. Below we describe the
        file structure.
      </p>
      <p>
        There are two HDUs, the <tt>PRIMARY</tt> which for the moment will be empty and a binary
        table which contains the actual <tt>CATALOGUE</tt>:
        <pre>
          <code>
            No.    Name      Ver    Type      Cards   Dimensions   Format
            0  PRIMARY       1 PrimaryHDU       4   ()
            1  CATALOGUE     1 BinTableHDU     67   22771R x 21C   [22A, E, E, 12A, 13A, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E, E]
          </code>
        </pre>
        The first three columns contain identification and exposure information:
        <pre>
          <code>
            name = 'ID'; format = '22A'
            name = 'RECORD'; format = 'E'
            name = 'NEXP'; format = 'E'
          </code>
        </pre>
        The following four columns contain the right ascention, the declination, the distance and
        the \(B-V\) color excess:
        <pre>
          <code>
            name = 'RA'; format = '12A'; unit = 'HH:MM:SS.ss'
            name = 'DEC'; format = '13A'; unit = 'DD:MM:SS.ss'
            name = 'DIST'; format = 'E'; unit = 'kpc'
            name = 'EBV'; format = 'E'; unit = 'mag'
          </code>
        </pre>
        The next three columns contain the volume correction factor, the signal-to-noise ratio
        across the wavelength range and the flux normalization computed as the median flux within
        the 5490 <span>&#8212;</span> 5510<span>&#8491;</span> wavelength windows:
        <pre>
          <code>
            name = 'VCORR'; format = 'E'
            name = 'SNR'; format = 'E'
            name = 'FNORM'; format = 'E'; unit = 'erg/s/cm^2/AA'
          </code>
        </pre>
        The next five columns contain the photometric magnitude in the SDSS <span>ugriz</span>
        passbands:
        <pre>
          <code>
            name = 'USDSS'; format = 'E'; unit = 'mag'
            name = 'GSDSS'; format = 'E'; unit = 'mag'
            name = 'RSDSS'; format = 'E'; unit = 'mag'
            name = 'ISDSS'; format = 'E'; unit = 'mag'
            name = 'ZSDSS'; format = 'E'; unit = 'mag'
          </code>
        </pre>
        The next two columns represent the radial velocity and its associated error as computed by
        the MaStar collaboration:
        <pre>
          <code>
            name = 'RADVEL'; format = 'E'; unit = 'km/s'
            name = 'RVERR'; format = 'E'; unit = 'km/s'
          </code>
        </pre>
        And the remaining columns represent the physical properties derived by CoSHA:
        <pre>
          <code>
            name = 'TEFF'; format = 'E'; unit = 'K'
            name = 'LOGG'; format = 'E'; unit = 'log/cm/s^2'
            name = 'MET'; format = 'E'; unit = '[Fe/H]'
            name = 'ALPHAM'; format = 'E'; unit = '[alpha/M]'
          </code>
        </pre>
        We plan to include the uncertainties associated with these quantities in a future release of
        this catalogue. In the mean time, if you use this catalogue, please cite
        <a href="#">Mejía-Narváez et al., 2021</a>. If you have some requests or complaints please
        send me an <a href="mailto:amejia@astro.unam.mx">e-mail</a>.
      </p>

      <hr>

      <h2 id="add">Changelog</h2>
      <p>
        <ul>
          <li><tt>v1.0.0</tt>: First version.</li>
        </ul>
      </p>
    </div>
  </body>
</html>
