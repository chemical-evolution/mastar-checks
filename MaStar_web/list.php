<?

extract($_REQUEST);
$n=0;
 include("db_def.php");
 $db=mysql_connect($dbhost, $dbusuario, $dbpassword);
 mysql_select_db($db_connect,$db);

 $sql="select * from Pipe3D_HIIregions order by col1";
 $result=mysql_query($sql,$db);
 while ($myrow=mysql_fetch_array($result)){
   $califa_name=str_replace(' ','',$myrow["col1"]);
     $redshift[$n]=substr($myrow["col15"],0,8);
     $Mass[$n]=substr($myrow["col2"],0,5);
     $SFR[$n]=substr($myrow["col3"],0,5);
   if (trim($califa_name)) {   
     $val=strlen($califa_name); 
//     print "'$n','$califa_name','$val' *** \n";

     if ($val>1) {
    $filename[$n]=$califa_name;
    $name[$n]=$califa_name;     


    $n++;
}
   }
  }

$np=intval($n/15);
$page=$start+1;
$max_page=$np+1;


?>

<html>
<head>
<title>CALIFA HIIregions</title>
<link rel="stylesheet" href="index.css" >

<style>
hr {
color: #FFFF00;
}
</style>

</head>
<body BGCOLOR=#FFFFFF leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" >
<div>
<table width=1024 BORDER=1 CELLPADDING=0 CELLSPACING=0>
<tr valign="top">
<td bgcolor="#EEEEEE" align="left">eCALIFA: <? echo $n ?> cubes</td>
<td colspan="2" align="right"  bgcolor="#EEEEEE">
<span class="title_black">
Select other page:
<select onchange="if (this.value) window.location.href=this.value">
<option value="list.php?start=0">pg. <? echo $start ?></option>
<?
 for ($i=0;$i<$max_page;$i++) {
 $ii=$i+1;
?>
<option value="list.php?start=<? echo $i ?>">pg. <? echo $i ?></option>
</a> &nbsp;
<?
 }
?>
</select>
</span>
</td>
</tr>
<tr>
 <td bgcolor="#EEEEEE" width="200">Galaxy Info</td>
 <td bgcolor="#EEEEEE">SDSS r,g,b image ;  Datacube r,g,b image; Emission line image; Segmentation Map</td>
 <td bgcolor="#EEEEEE" width="200">HII regions catalog</td>
</tr>
<?
$max=15*$start+15;
$min=15*$start;
if ($max>$n) {
   $max=$n;
}

 for ($i=$min;$i<$max;$i++) {


?>
<tr><td width="200" valign="top">
<table border=0 width="200" height="100" valign="top">
 <tr><td width="50">NAME:</td><td width="150" valign="top"><? echo $name[$i] ?></td></tr>
 <tr><td width="50">z :</td><td width="150" valign="top"><? echo $redshift[$i] ?></td></tr>
 <tr><td width="50">log(M):</td><td width="150" valign="top"><? echo $Mass[$i] ?></td></tr>
 <tr><td width="50">log(SFR):</td><td width="150" valign="top"><? echo $SFR[$i] ?></td></tr>
</table>
</td>
<td width=445  valign="top"><a href="/CALIFA/V500/images/<? echo $name[$i] ?>_SDSS.jpg"><img src="/CALIFA/V500/images/<? echo $name[$i] ?>_SDSS.jpg" width=110 height=110><a href="/CALIFA/V500/images/RGB_cont_<? echo $name[$i] ?>.png"><img src="/CALIFA/V500/images/RGB_cont_<? echo $name[$i] ?>.png" width=110 height=110><a href="/CALIFA/V500/images/RGB_gas_<? echo $name[$i] ?>.png"><img src="/CALIFA/V500/images/RGB_gas_<? echo $name[$i] ?>.png" width=110 height=110></a><a href="figures/seg_map.<? echo $name[$i] ?>.png"><img src="figures/seg_map.<? echo $name[$i] ?>.png" width=150></a><a href="figures/BPT.<? echo $name[$i] ?>.png"><img src="figures/BPT.<? echo $name[$i] ?>.png" width=300></a>
</td>
<td width="300" valign="top">
Diff. Corr. Emission line fluxes: <a href="new_catalogs/HII.<? echo $name[$i] ?>.flux_elines.diff_corr.csv">CSV</a>/<a href="new_fitsfiles/HII.<? echo $name[$i] ?>.flux_elines.diff_corr.fits">FITs</a><br>
Orig. Emission line fluxes: <a href="new_catalogs/HII.<? echo $name[$i] ?>.flux_elines.csv">CSV</a>/<a href="new_fitsfiles/HII.<? echo $name[$i] ?>.flux_elines.fits">FITs</a><br>
Prop. of the stellar populations: <a href="new_catalogs/HII.<? echo $name[$i] ?>.SSP.csv">CSV</a>/<a href="new_fitsfiles/HII.<? echo $name[$i] ?>.SSP.fits">FITs</a><br>
Stellar decomposition weights: <a href="new_catalogs/HII.<? echo $name[$i] ?>.SFH.csv">CSV</a>/<a href="new_fitsfiles/HII.<? echo $name[$i] ?>.SFH.fits">FITs</a><br>
<a href="new_catalogs/seg_Ha.<? echo $name[$i] ?>.fits.gz">Segmentation masks</a><br>
<a href="new_catalogs/mask_Ha.<? echo $name[$i] ?>.fits.gz">Hii regions mask</a><br>
</td>
</tr>
<?

}
?>


</table>
</html>